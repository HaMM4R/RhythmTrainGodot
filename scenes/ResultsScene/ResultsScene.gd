extends PanelContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var noteLabel = $MarginContainer/VBoxContainer/HBoxContainer2/VBoxContainer/HBoxContainer4/NotesHitLabel
onready var streakLabel = $MarginContainer/VBoxContainer/HBoxContainer2/VBoxContainer/HBoxContainer/StreakLabel
onready var accLabel = $MarginContainer/VBoxContainer/HBoxContainer2/VBoxContainer/HBoxContainer3/AccLabel

onready var star1Tween = $MarginContainer/VBoxContainer/HBoxContainer2/HBoxContainer/star1Tween
onready var star2Tween = $MarginContainer/VBoxContainer/HBoxContainer2/HBoxContainer/star2Tween
onready var star3Tween = $MarginContainer/VBoxContainer/HBoxContainer2/HBoxContainer/star3Tween

onready var star1 = $MarginContainer/VBoxContainer/HBoxContainer2/HBoxContainer/star1
onready var star2 = $MarginContainer/VBoxContainer/HBoxContainer2/HBoxContainer/star2
onready var star3 = $MarginContainer/VBoxContainer/HBoxContainer2/HBoxContainer/star3

#ONLY FOR DEBUG
onready var starLabel = $MarginContainer/VBoxContainer/HBoxContainer2/VBoxContainer/HBoxContainer5/StarLabel

onready var continueButton = $MarginContainer/VBoxContainer/MainMenuButton

var notesHit = 0
var score = 0
var songID
var songPassed
var streak = 0
var accuracy = 0
var totalNotes = 0
var songFailed = false

var starsAwarded = 0
var newStars = 0 

# Called when the node enters the scene tree for the first time.
func _ready():
	noteLabel.text = String(score)
	streakLabel.text = String(streak)
	accLabel.text = String(accuracy)
	
	if starsAwarded >= 1:
		star1Tween.interpolate_property(star1, 'rect_rotation',
			-360.0, 0.0, 2.0,
			Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
		
		star1Tween.interpolate_property(star1, 'modulate',
			Color("00ffffff"), Color("ffffff"), 2.0,
			Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
		star1Tween.start()

func _process(delta):
	pass
	#print(star1.rect_rotation)
	#print(star1.modulate)

func init(var scoreIn, var streakIn, var accuracyIn, var notesHitIn, var songIDIn, var songPassedIn, var totalNotesIn, var songFailedIn):
	score = scoreIn
	notesHit = notesHitIn
	streak = streakIn
	accuracy = accuracyIn
	songID = songIDIn
	songPassed = songPassedIn
	totalNotes = totalNotesIn
	songFailed = songFailedIn
	
	
	if(!songFailed):
		var hitPercentage = float(notesHit) / float(totalNotes)
		
		#Calculate hit percentage to work out how many stars to award
		if hitPercentage > 0.75:
			starsAwarded = 3
		elif hitPercentage > 0.5:
			starsAwarded = 2
		elif hitPercentage > 0.25:
			starsAwarded = 1
	
		$MarginContainer/VBoxContainer/HBoxContainer2/VBoxContainer/HBoxContainer5/StarLabel.text = String(starsAwarded)
		newStars = Save.saveSongDetails(songID, starsAwarded, score)
	else:
		$MarginContainer/VBoxContainer/Label.text = "Good Effort!"

	Sql.completed_exercise(score,accuracy,streak,notesHit,songID,songPassed)
	Sql.connect("completed_exercise", self, "displayContinue")


#Display continue after results are stored in database
func displayContinue():
	continueButton.show()

	
func _on_Button_pressed():
	queue_free()
	
	#Set leaderboard depending on which group you're in
	if PlayerInfo.leaderboardGroup == 1:
		get_tree().change_scene("res://scenes/MainNew/Main.tscn")
	else:
		var leaderboardScreen = load("res://scenes/Leaderboard/Leaderboard.tscn")
		var leaderboardInstance = leaderboardScreen.instance()
		leaderboardInstance.init(songID)
		get_tree().root.call_deferred("add_child", leaderboardInstance)


var timerCount = 0

func _on_StarTimer_timeout():
	
	if timerCount == 0 and starsAwarded >= 2:
		
		print("Star 2")
		star2Tween.interpolate_property(star2, 'rect_rotation',
		-360.0, 0.0, 2.0,
		Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
		star2Tween.interpolate_property(star2, 'modulate',
		Color("00ffffff"), Color("ffffff"), 2.0,
		Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
		star2Tween.start()
		timerCount+=1
		
	elif timerCount == 1 and starsAwarded >= 3:
			
		print("Star 3")
		star3Tween.interpolate_property(star3, 'rect_rotation',
		-360.0, 0.0, 2.0,
		Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
		
		star3Tween.interpolate_property(star3, 'modulate',
		Color("00ffffff"), Color("ffffff"), 2.0,
		Tween.TRANS_QUAD, Tween.EASE_IN_OUT)
		star3Tween.start()
		$MarginContainer/VBoxContainer/HBoxContainer2/HBoxContainer/StarTimer.stop()
