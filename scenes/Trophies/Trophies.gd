extends Node2D

onready var trophies = [
	$Trophy1,
	$Trophy2,
	$Trophy3,
	$Trophy4,
	$Trophy5,
	$Trophy6,
	$Trophy7,
	$Trophy8,
	$Trophy9,
	$Trophy10
]

var setbuttons = []
var viewButtons = []
var progresses = []
var curChallengeIndex = 0
func _ready():
	$clicks.play()
	$ChallengeInfo.hide()
	setbuttons = get_tree().get_nodes_in_group("setbutton")
	
	for i in range(0, setbuttons.size()):
		setbuttons[i].connect("pressed", self, "iconSet", [i])

	viewButtons = get_tree().get_nodes_in_group("viewChallenge")
	for i in range(0, setbuttons.size()):
		viewButtons[i].connect("pressed", self, "viewChallenge", [i])
		
		
	progresses = get_tree().get_nodes_in_group("challengePercentage")
	for i in range(progresses.size()):
		trophies[i].get_node("ActiveLabel").hide()
		progresses[i].value = Save.returnChallengeDetails(ChallengeManager.challengeNames[i])
		if(progresses[i].value >= 100):
			trophies[i].get_node("Locked").hide()

		if(Save.returnActiveChallenge() == ChallengeManager.challengeNames[i] && progresses[i].value < 100):
			trophies[i].get_node("Locked").hide()
			trophies[i].get_node("ActiveLabel").show()
			
	setStateToDefault()
	getIcon(Save.returnActiveTrophy())

func iconSet(index):
	Sql.update_badge(index)
	Save.saveActiveTrophy(index)
	setStateToDefault()
	$clicks.play()
	trophies[index].get_node("Set").hide()
	trophies[index].get_node("Label").text = "Chosen"
	trophies[index].get_node("Label").show()
	trophies[index].get_node("View").hide()
	trophies[index].get_node("progress").hide()


func viewChallenge(index):
	curChallengeIndex = index
	$ChallengeInfo.show()
	$ChallengeInfo/ChallengeDesc.text = ChallengeManager.challengeDescriptions[index]
	$ChallengeInfo/SetActive.show()
	$ChallengeInfo/GotoExercises.hide()
	$ChallengeInfo/GotoSongs.hide()
	
	if(ChallengeManager.challengeMode[index] == "Practice"):
		$ChallengeInfo/GotoExercises.show()
	else:
		$ChallengeInfo/GotoSongs.show()
	
	if(ChallengeManager.challengeNames[index] == Save.returnActiveChallenge()):
		$ChallengeInfo/SetActive.hide()
		$ChallengeInfo/ActiveLabel.show()


func setStateToDefault():
	for i in range(0,trophies.size()):
		trophies[i].get_node("Label").text = "Locked"
		trophies[i].get_node("badge").texture = ChallengeManager.challengeBadge[i]
		trophies[i].get_node("challenge").text = ChallengeManager.challengeNames[i]
		trophies[i].get_node("badge").modulate = Color(1,1,1,0.3)
		trophies[i].get_node("Set").hide()
		trophies[i].get_node("Label").hide()
		trophies[i].get_node("View").show()
		trophies[i].get_node("progress").show()
		
		if(Save.returnChallengeDetails(ChallengeManager.challengeNames[i]) >= 100):
			trophies[i].get_node("badge").modulate = Color(1,1,1,1)
			trophies[i].get_node("Set").show()
			trophies[i].get_node("Label").hide()
			trophies[i].get_node("View").hide()
			trophies[i].get_node("progress").hide()

func getIcon(index):
	if index == -1:
		return
		
	trophies[index].get_node("Set").hide()
	trophies[index].get_node("Label").show()
	trophies[index].get_node("Label").text = "Chosen"
	

func _on_Back_pressed():
	get_tree().change_scene("res://scenes/MainNew/Main.tscn")


func _on_Close_pressed():
	$ChallengeInfo.hide()
	$clicks.play()


func _on_SetActive_pressed():
	$clicks.play()
	$ChallengeInfo/SetActive.hide()
	$ChallengeInfo/ActiveLabel.show()
	Save.saveActiveChallenge(ChallengeManager.challengeNames[curChallengeIndex])
	var progress = 0
	for i in range(trophies.size()):
		progress = Save.returnChallengeDetails(ChallengeManager.challengeNames[i])
		if(progress < 100):
			trophies[i].get_node("ActiveLabel").hide()
			trophies[i].get_node("Locked").show()
		if(Save.returnActiveChallenge() == ChallengeManager.challengeNames[i]):
			trophies[i].get_node("Locked").hide()
			trophies[i].get_node("ActiveLabel").show()

func _on_GotoSongs_pressed():
	get_tree().change_scene("res://scenes/ChooseSong/ChooseSong.tscn")


func _on_GotoExercises_pressed():
	get_tree().change_scene("res://scenes/ChooseExercise/ChooseExercise.tscn")
