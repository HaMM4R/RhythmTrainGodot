extends Node2D

onready var nameLabel = $NameLabel
onready var activeExercise = $Challenges/NoChallengeLabel
onready var activeProgressBar = $Challenges/ProgressBar
onready var completedLabel = $Challenges/Completed

#Account creation
var accountCreateButtons = []
var accountSwitchButtons = []
onready var accountUI = [$AccountSetup/Account1, $AccountSetup/Account2, $AccountSetup/Account3]

# Called when the node enters the scene tree for the first time.
func _ready():
	#Sql.removed_save()
	
	if(PlayerInfo.activeSave == ""):
#		PlayerInfo.activeSave = "user://savegame0.save"
		$PlayButton.hide()
		$Practice.hide()
		$Trophies.hide()
		$Challenges.hide()
		$LOADING.show()
		$AccountSetup.hide()
		$Challenges.hide()
		$levelProgress.hide()
		$curentLevel.hide()
		$nextLevel.hide()
		$NameLabel.hide()
		$Help2.hide()
		$SwapAccount.hide()
		loadAccounts()
		$AccountSetup.show()
		$AccountSetup/CloseAccount.hide()
		for i in range(0,3):
			accountUI[i].get_node("Switch").text = "Select"
	#	init()
	else:
		init()
	
	for i in range(0,3):
		if(accountUI[i].get_node("Name").text == PlayerInfo.playerName):
			accountUI[i].get_node("Active").show()
			accountUI[i].get_node("Switch").hide()
	
	accountCreateButtons = get_tree().get_nodes_in_group("createUser")
	for i in range(0, accountCreateButtons.size()):
		accountCreateButtons[i].connect("pressed", self, "accountCreated", [i])

	accountSwitchButtons = get_tree().get_nodes_in_group("switchUser")
	for i in range(0, accountSwitchButtons.size()):
		accountSwitchButtons[i].connect("pressed", self, "switchAccounts", [i])
		
	Bayes.calcCoefficients(10)
	
	
var returningPlayer
func init():
	$HelpPanel.hide()
	$clicks.play()
	$Panel.show()
	$Challenges.show()
	returningPlayer = Sql.signup_player(PlayerInfo.activeSave, PlayerInfo.activeDifficulties)
	
	if(!returningPlayer):
		$HelpPanel.show()
		$HelpPanel/Title.text = "Welcome!"
	
	#setName()
	Sql.connect("player_saved", self, "setName")
	
	$PlayButton.hide()
	$Practice.hide()
	$Trophies.hide()
	$Challenges.hide()
	$LOADING.show()
	$AccountSetup.hide()
	loadAccounts()

func setName():
	$levelProgress.show()
	$curentLevel.show()
	$nextLevel.show()
	$NameLabel.show()
	$Help2.show()
	$SwapAccount.show()
	$PlayButton.show()
	$Practice.show()
	$Trophies.show()
	$Challenges.show()
	$LOADING.hide()
	
	if PlayerInfo.playerName != null:
		nameLabel.text = "Welcome, " + PlayerInfo.playerName + "!"	
			
	#Sets the level of the player
	for i in range (0, PlayerInfo.xpForLevel.size()):
		if (PlayerInfo.totalPreviousScore < PlayerInfo.xpForLevel[i]):
			break
		PlayerInfo.currentLevel = i + 1
		
	Sql.update_level(PlayerInfo.currentLevel)
	setChallengeProgress()
	setBadgeIcon()
	
	$curentLevel.text = String(PlayerInfo.currentLevel)
	if(PlayerInfo.currentLevel < 9):
		$nextLevel.text = String(PlayerInfo.currentLevel + 1)
	else:
		$nextLevel.text = String(PlayerInfo.currentLevel)
		
	var xpToLevel = 0
	if(PlayerInfo.currentLevel < 9):
		xpToLevel = PlayerInfo.xpForLevel[PlayerInfo.currentLevel + 1] - PlayerInfo.totalPreviousScore
	else:
		xpToLevel = 0
		
	$levelProgress/xpmarker.text = String(xpToLevel) + "xp to level up!"
	if(PlayerInfo.currentLevel < 9):
		$levelProgress.value = float(PlayerInfo.totalPreviousScore) / PlayerInfo.xpForLevel[PlayerInfo.currentLevel + 1] * 100 
	else:
		$levelProgress.value = 100
	
	
func setChallengeProgress():
	completedLabel.hide()
	if(Save.returnActiveChallenge() == ""):
		activeProgressBar.hide()
		$Challenges/NoChallengeLabel.show()
	else:
		$Challenges/NoChallengeLabel.show()
		activeProgressBar.show()
		var activeChallenge = Save.returnActiveChallenge()
		var index = ChallengeManager.challengeNames.find(activeChallenge)
		activeExercise.text = ChallengeManager.challengeDescriptions[index]
		var progress = Save.returnChallengeDetails(activeChallenge)
		activeProgressBar.value = progress
		
		if(progress >= 100):
			activeProgressBar.hide()
			completedLabel.show()
			


func setBadgeIcon():
	var index = Save.returnActiveTrophy()
	if index == -1:
		return 
		
	$badge.texture = ChallengeManager.challengeBadge[index]


func _on_PlayButton_pressed():
	get_tree().change_scene("res://scenes/ChooseSong/ChooseSong.tscn")

func _on_Practice_pressed():
	get_tree().change_scene("res://scenes/ChooseExercise/ChooseExercise.tscn")

func _on_Challenges_pressed():
	get_tree().change_scene("res://scenes/Challenges/ChallengesNew.tscn")


func _on_Trophies_pressed():
	get_tree().change_scene("res://scenes/Trophies/Trophies.tscn")


func _on_Help_pressed():
	$HelpPanel.show()
	$clicks.play()

func _on_HelpDismiss_pressed():
	$HelpPanel.hide()
	$clicks.play()

var accountIndexToCreate = 0

func loadAccounts():
	for i in range(0, 3):
		var save_game = File.new()
		if save_game.file_exists("user://savegame" + str(i) + ".save"):
			accountUI[i].get_node("Create").hide()
			accountUI[i].get_node("Active").hide()
			save_game.open("user://savegame" + str(i) + ".save", File.READ)
			while save_game.get_position() < save_game.get_len():
				# Get the saved dictionary from the next line in the save file
				var node_data = parse_json(save_game.get_line())
				accountUI[i].get_node("Name").text = node_data["name"]
				break
	
func _on_SwapAccount_pressed():
	$AccountSetup.show()
	for i in range(0,3):
		accountUI[i].get_node("Switch").text = "Switch"
	$AccountSetup/CloseAccount.show()
	
func _on_AccountClose_pressed():
	$AccountSetup/CheckCreate.hide()	

func _on_AccountCreateButton_pressed():
	$AccountSetup/CheckCreate.hide()
	accountUI[accountIndexToCreate].get_node("Create").hide()
	for i in range(0, 3):
		accountUI[i].get_node("Active").hide()
		accountUI[i].get_node("Switch").show()
		
	accountUI[accountIndexToCreate].get_node("Active").show()
	accountUI[accountIndexToCreate].get_node("Switch").hide()
	
	print("account index, ", accountIndexToCreate)
	
	#"user://savegame.save"
	PlayerInfo.activeSave = "user://savegame" + str(accountIndexToCreate) + ".save"
	PlayerInfo.activeDifficulties = "user://difficulties" + str(accountIndexToCreate) + ".save"	
	#var returningPlayer = Sql.signup_player(PlayerInfo.activeSave, PlayerInfo.activeDifficulties)
	#Sql.connect("player_saved", self, "setName")
	Sql.connect("player_saved", self, "setAccountLabel", [accountIndexToCreate])
	$AccountSetup.hide()
	init()
	
func setAccountLabel(index):
	#accountUI[index].get_node("Name").text = PlayerInfo.playerName
	loadAccounts()

func _on_CloseAccount_pressed():
	$AccountSetup.hide()

func accountCreated(index):
	accountIndexToCreate = index
	$AccountSetup/CheckCreate.show()

func switchAccounts(index):
	for i in range(0, 3):
		accountUI[i].get_node("Active").hide()
		accountUI[i].get_node("Switch").show()
	accountUI[index].get_node("Active").show()
	accountUI[index].get_node("Switch").hide()
	
	$Challenges/NoChallengeLabel.text = "Go to the trophies menu to set an active misson!"
	PlayerInfo.activeSave = "user://savegame" + str(index) + ".save"	
	PlayerInfo.activeDifficulties = "user://difficulties" + str(index) + ".save"	
	var returningPlayer = Sql.signup_player(PlayerInfo.activeSave, PlayerInfo.activeDifficulties)
	Sql.connect("player_saved", self, "setName")
	$AccountSetup.hide()
	init()
