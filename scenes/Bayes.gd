extends Node

var loadedResults = []
var loadedIDs = []
var skill = []

var skillResults = []

var pdf = []
var dataPoints = returnBetaRange()

var numOfPreviousUpdatesToConsider = 10
var coefficients = [] 

var correlations = [] 
var initialRhythmData = [] 

func rhythmPlayed(id, score):
	var bID = str(id)
	var updatedBeta = []	
	
	var a = 1
	var b = 1
	var updateN = 1
	var updatek = 0
	var index = 0
	
	if(loadedIDs.has(bID)):
		if(score > 0.7):
			updatek = 1
			
		var mean = 0
		var stdev = 0 
		
		####### HANDLE SKILL ESTIMATION #########
		handleSkill(updatek)
		
		######## HANDLE RHYTHM ESTIMATION ##########
		index = loadedIDs.find(bID, 0)
		a = loadedResults[index][0]
		b = loadedResults[index][1]
		
		loadedResults[index][4].append(updatek)
					
		updatedBeta = updateBeta(a, b, updateN, updatek)
		a = float(updatedBeta[0])
		b = float(updatedBeta[1])
		
		mean = returnMean(a,b)
		stdev = returnStdDev(a,b)
		pdf = returnBetaPDF(dataPoints, a, b)
		
		loadedResults[index][0] = a
		loadedResults[index][1] = b
		loadedResults[index][2] = mean
		loadedResults[index][3] = stdev

	return index

	
#NEED TO UNIT TESTTESTESTESTSETSET
#NEED TO UNIT TEST 
#NEED TO UNIT TEST
#NEED TO UNIT TEST
func handleSkill(kIn):
	if(Bayes.skill[4].size() + 1 > 25):
		for i in range(0, 10):
			Bayes.skill[4].pop_front()
	
	var a = Bayes.skill[0]
	var b = Bayes.skill[1]
	var n = Bayes.skill[4].size() + 1
	var k = kIn
	
	for x in range(0, skill[4].size()):
		if skill[4][x] == 1:
			k += 1
			
	var percentCorrect = float(k) / float(n)
	
	if(n > 5):
		n = 5
		
	k = int(n * percentCorrect)
	
	var updatedBeta = updateBeta(1, 1, n, k)
	var mean = returnMean(a,b)
	var stdev = returnStdDev(a,b)
	
	Bayes.skill[0] = updatedBeta[0]
	Bayes.skill[1] = updatedBeta[1]
	Bayes.skill[2] = mean
	Bayes.skill[3] = stdev
	Bayes.skill[4].append(kIn)
	
func recalculateInitialBeliefs():
	var updatedBeta = []
	var N = 0; var k = 0
	var a = 0; var b = 0
	var mean = 0
	var stdev = 0
	
	print("THIS IS GETTING CALLED")
	
	for i in range(0, loadedResults.size()):
		if(loadedResults[i][4].size() < numOfPreviousUpdatesToConsider):
			return
		
		print("AN ARRAY HAD A VALUE GREATER THAN 10")
		print("LOADED RESULTS, ", loadedResults[i])
		
		for x in range(0, loadedResults[i][4].size()):
			loadedResults[i][5].append(loadedResults[i][4][x])
		
		while(loadedResults[i][4].size() > numOfPreviousUpdatesToConsider):
			loadedResults[i][4].pop_front()
			
		N = loadedResults[i][4].size()
		k = 0
		
		#WHY ON EARTH CANT I GET THE ARRAY COUNT FUNCTION TO WORK ON THIS DAMN BAYES ARRAY
		for x in range(0, loadedResults[i][4].size()):
			if loadedResults[i][4][x] == 1:
				k += 1

		updatedBeta = updateBeta(1, 1, N, k)
		a = float(updatedBeta[0])
		b = float(updatedBeta[1])
		
		print("THIS IS NEW N ", N)
		print("THIS IS NEW K ", k)
		
		mean = returnMean(a,b)
		stdev = returnStdDev(a,b)
		pdf = returnBetaPDF(dataPoints, a, b)
		
		loadedResults[i][0] = a
		loadedResults[i][1] = b
		loadedResults[i][2] = mean
		loadedResults[i][3] = stdev
		
		loadedResults[i][4].clear()
		Save.saveDifficulties(PlayerInfo.activeDifficulties)
		
		print("LOADED RESULTS POST REMOVAL OF OLD ENTRIES, ", loadedResults[i])

func returnMean(a, b):
	return (float(a)/(float(a)+float(b)))
#
func returnStdDev(a, b):
	return sqrt((float(a)*float(b)) / (pow((float(a)+float(b)), 2) * (float(a)+float(b)+1)))  


func returnBetaRange():
	var out = []
	for i in range(0, 101):
		out.append(0.01 * i)
	return out
	
#Uses The Spouge approximation
func returnGamma(coefficients, x):
	var numOfCoeffs = coefficients.size()
	var accm = coefficients[0]
	for i in range(0, numOfCoeffs):
		accm += coefficients[i] / (x + i)
	accm *= exp(-(x+numOfCoeffs)) * pow(x+numOfCoeffs, x+0.5)
	return accm/x


func calcCoefficients(numOfCoefficients):
	var c = []
	var k1_factrl = 1.0 
	c.append(sqrt(2.0 * PI))
	for i in range(1,numOfCoefficients):
		c.append(exp(numOfCoefficients-i) * pow(numOfCoefficients-i, i-0.5) / k1_factrl)
		k1_factrl *= float(-i);
	coefficients = c
	print(coefficients)

func returnBetaPDF(data, alpha, beta):
	var out = []
	var coeffs = coefficients

	var alphaGamma = returnGamma(coeffs, alpha)
	var betaGamma = returnGamma(coeffs, beta)
	var alphaBetaGamma = returnGamma(coeffs, alpha+beta)
	
	for x in data:
		out.append((float(pow(x,alpha-1)) * float(pow((1-x),(beta-1))) * float(alphaBetaGamma) / float(alphaGamma) / float(betaGamma)))
	return out

func updateBeta(alphaPrior, betaPrior, N, k):
	var alpha_post = float(alphaPrior) + float(k)
	var beta_post = float(betaPrior) + float(N) - float(k)
	return [float(alpha_post), float(beta_post)]
	
#FUSE BETAS
#TAKES 2 dists as inputs and return beta as output
#JUST ADDS UP A AND B VALUES
func returnFusedSkillandEstimate():
	skillResults.clear()
	var alphaNew = 0
	var betaNew = 0
	for i in range(0, loadedResults.size()):
		alphaNew = loadedResults[i][0] + skill[0]
		betaNew = loadedResults[i][1] + skill[1]
		skillResults.append([alphaNew,betaNew,returnMean(alphaNew,betaNew),returnStdDev(alphaNew,betaNew)])



func calculateCorrelations():
	for i in range(initialRhythmData.size()):
		var holder = []
		for j in range(initialRhythmData.size()):
			var correlation = godfreyFoxEstimator(initialRhythmData[i], initialRhythmData[j])
			var alpha = calculateAlphaFromMeanAndVariance(correlation, 0.2)
			var beta = calculateBetaFromMeanAndVariance(correlation, 0.2)
			holder.append(alpha)
			holder.append(beta)
		correlations.append(holder)
	print(correlations)
			
func godfreyFoxEstimator(d1, d2):
	var s = 0
	for i in range(d1.size()):
		s += (d1[i] * d2[i])

	return float(((float(1) / d1.size() * float(s)) + float(1)) / float(2))

func calculateAlphaFromMeanAndVariance(mean, variance):
	return -(mean * (pow(variance, 2) + pow(mean,2) - mean) / pow(variance, 2))

func calculateBetaFromMeanAndVariance(mean, variance): 
	return (((pow(variance, 2) + pow(mean,2)- mean) * (mean - 1)) / pow(variance,2))



