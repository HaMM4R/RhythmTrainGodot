extends Node

var save_game = File.new()

var activeSaveFile = ""
var activeDifficultyFile = ""

#Save results of song
func saveSongDetails(songID, starsAwarded, score):
	save_game.open(activeSaveFile, File.READ)
	
	var text = save_game.get_as_text()
	var saveJson = parse_json(text)
	var newStarsAchieved = 0 
	
	while save_game.get_position() < save_game.get_len():
		# Get the saved dictionary from the next line in the save file
		var node_data = parse_json(save_game.get_line())
		#Check to see if star entry exists, if so make sure new rating is bigger
		if(node_data.has(String(songID))):
			if starsAwarded > node_data[String(songID)]:
				saveJson[String(songID)] = starsAwarded
				newStarsAchieved = starsAwarded - node_data[String(songID)]
				saveJson["totalStars"] += newStarsAchieved
				save_game.close()
			break
		#Add entry if doesn't exist
		else:
			saveJson[String(songID)] = starsAwarded
			newStarsAchieved = starsAwarded
			saveJson["totalStars"] += newStarsAchieved
			save_game.close()
			
	#Get ready to save score
	save_game.open(activeSaveFile, File.READ)
	while save_game.get_position() < save_game.get_len():
		var node_data = parse_json(save_game.get_line())
		if(node_data.has("totalScore")):
			var scoreToSave = score + int(node_data["totalScore"])
			saveJson["totalScore"] = scoreToSave
		
		
		
	saveStats(saveJson)
	Sql.save_total_stars(PlayerInfo.totalStars + newStarsAchieved)
	return newStarsAchieved

#Return number of stars for specific song
func returnStars(songID):
	var stars = -1
	save_game.open(activeSaveFile, File.READ)
	while save_game.get_position() < save_game.get_len():
		# Get the saved dictionary from the next line in the save file
		var node_data = parse_json(save_game.get_line())
		if(node_data.has(String(songID))):
			stars = node_data[String(songID)]
			save_game.close()
			return stars
	return stars


func saveChallengeDetails(challenge, progress):
	if(progress > 100):
		progress = 100
	
	save_game.open(activeSaveFile, File.READ)
	var text = save_game.get_as_text()
	var saveJson = parse_json(text)
	
	while save_game.get_position() < save_game.get_len():
		# Get the saved dictionary from the next line in the save file
		var node_data = parse_json(save_game.get_line())
		#Check to see if star entry exists, if so make sure new rating is bigger
		if(node_data.has(String(challenge))):
			saveJson[String(challenge)] = progress
			save_game.close()
			break
		#Add entry if doesn't exist
		else:
			saveJson[String(challenge)] = progress
			save_game.close()
				
	saveStats(saveJson)

#Sets the players current active challenge
func saveActiveChallenge(challenge):
	save_game.open(activeSaveFile, File.READ)
	var text = save_game.get_as_text()
	var saveJson = parse_json(text)
	
	while save_game.get_position() < save_game.get_len():
		# Get the saved dictionary from the next line in the save file
		var node_data = parse_json(save_game.get_line())
		#Set active challenge
		saveJson["active_challenge"] = challenge
		save_game.close()
				
	saveStats(saveJson)

#Returns their current challenge
func returnActiveChallenge():
	var activeChallenge = ""
	save_game.open(activeSaveFile, File.READ)
	while save_game.get_position() < save_game.get_len():
		# Get the saved dictionary from the next line in the save file
		var node_data = parse_json(save_game.get_line())
		if(node_data.has("active_challenge")):
			activeChallenge = node_data["active_challenge"]
			save_game.close()
			return activeChallenge
	return activeChallenge

func returnChallengeDetails(challenge):
	var progress = 0
	save_game.open(activeSaveFile, File.READ)
	while save_game.get_position() < save_game.get_len():
		# Get the saved dictionary from the next line in the save file
		var node_data = parse_json(save_game.get_line())
		if(node_data.has(String(challenge))):
			progress = node_data[String(challenge)]
			save_game.close()
			return progress
	return progress


func saveActiveTrophy(trophy):
	save_game.open(activeSaveFile, File.READ)
	var text = save_game.get_as_text()
	var saveJson = parse_json(text)
	
	while save_game.get_position() < save_game.get_len():
		# Get the saved dictionary from the next line in the save file
		var node_data = parse_json(save_game.get_line())
		#Set active challenge
		saveJson["trophy"] = trophy
		save_game.close()
				
	saveStats(saveJson)

func returnActiveTrophy():
	var activeTrophy = -1
	save_game.open(activeSaveFile, File.READ)
	while save_game.get_position() < save_game.get_len():
		# Get the saved dictionary from the next line in the save file
		var node_data = parse_json(save_game.get_line())
		if(node_data.has("trophy")):
			activeTrophy = node_data["trophy"]
			save_game.close()
			return activeTrophy
	return activeTrophy


#Save player stats
func saveStats(saveJson):
	save_game.open(activeSaveFile, File.WRITE)
	save_game.seek_end()
	save_game.store_line(to_json(saveJson))
	save_game.close()



#Save a new user
func save_user(var saveData, playerSaveFile, difficultySave):
	activeSaveFile = playerSaveFile
	var save_game = File.new()
	save_game.open(activeSaveFile, File.WRITE)
	save_game.store_line(to_json(saveData))
	save_game.close()
	createDifficultyFiles(difficultySave)


#Load the users data
func load_user(playerSaveFile, difficultySave):
	activeSaveFile = playerSaveFile
	var save_game = File.new()
	save_game.open(activeSaveFile, File.READ)
	while save_game.get_position() < save_game.get_len():
		# Get the saved dictionary from the next line in the save file
		var node_data = parse_json(save_game.get_line())
		PlayerInfo.playerName = node_data["name"]
		Sql.get_player_info(PlayerInfo.playerName)
		break
		
	loadDifficulties(difficultySave)

func createDifficultyFiles(difficultySave):
	var save_game = File.new()
	if save_game.file_exists(difficultySave):
		return
	
	var testStr = str(0)
	var toSave = {
		testStr: [1, 1, 0, 1, [], []] #alpha, beta, mean, stdev, successes, previousSuccesses
	}
	
	save_game.open(difficultySave, File.WRITE)
	save_game.seek_end()
	save_game.store_line(to_json(toSave))
	save_game.close()

	var rhythms = GetFiles.getMusicJSON("randomBeats2.json")

	#Need to work out the a and b of the intial probability of the bars I think it should be
	#Use N = 10 and how many times I think they'll get it for k 
	
	################ NEED TO MIGRATE THIS INTO THE BAYES STUFF ###########################
	for i in range(0, rhythms["RandomBars"].size()):
		var updatedBeta = []	
		var a = 1
		var b = 1
		var N = 10
		var k = int((rhythms["RandomBars"][i]["difficulty"]))
		
		############### TESTING FOR CORRELATIONS ##########################
		var testSuccesses = []
		
		for j in range(N):
			if(j <= k):
				testSuccesses.append(1)
			else:
				testSuccesses.append(-1)

		Bayes.initialRhythmData.append(testSuccesses)
		#####################################################################

		updatedBeta = Bayes.updateBeta(a, b, N, k)
		a = float(updatedBeta[0])
		b = float(updatedBeta[1])
		
		var mean = Bayes.returnMean(a,b)
		var stdev = Bayes.returnStdDev(a,b)

		Bayes.loadedResults.append([a, b, mean, stdev, [], []])
		Bayes.loadedIDs.append(rhythms["RandomBars"][i]["id"])
	
	Bayes.calculateCorrelations()
	Bayes.skill = [1,1,1,1,[]]	
	print("BAYES SKILL IN CREEATE: ", Bayes.skill)
	saveDifficulties(difficultySave)

func update_difficulties():
	pass

func saveDifficulties(difficultySave):
	save_game.open(difficultySave, File.READ)
	var text = save_game.get_as_text()
	var saveJson = parse_json(text)
	
	while save_game.get_position() < save_game.get_len():
		var node_data = parse_json(save_game.get_line())
		saveJson["skill"] = Bayes.skill
		saveJson["correlations"] = Bayes.correlations
		print("BAYES SKILL IN SAVE, ", Bayes.skill)
		for i in range(0, Bayes.loadedIDs.size()):
			saveJson[String(Bayes.loadedIDs[i])] = Bayes.loadedResults[i]
	
	save_game.open(difficultySave, File.WRITE)
	save_game.seek_end()
	save_game.store_line(to_json(saveJson))
	save_game.close()

func loadDifficulties(difficultySave):
	Bayes.loadedIDs.clear()
	Bayes.loadedResults.clear()
	Bayes.skill.clear()
	save_game.open(difficultySave, File.READ)
	while save_game.get_position() < save_game.get_len():
		var node_data = parse_json(save_game.get_line())
		var keys = node_data.keys()
		for key in keys:
			if(key == "skill"):
				Bayes.skill = node_data[key]
				continue	
			if(key == "correlations"):
				Bayes.correlations = node_data[key]
				continue
			Bayes.loadedIDs.append(key)
			Bayes.loadedResults.append(node_data[key])
		


