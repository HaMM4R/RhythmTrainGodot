extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.

func _ready():
	$clicks.play()

func init(index):
	$Leaderboard.init(index)


func _on_Back_pressed():
	self.queue_free()
	get_tree().change_scene("res://scenes/ChooseSong/ChooseSong.tscn")
