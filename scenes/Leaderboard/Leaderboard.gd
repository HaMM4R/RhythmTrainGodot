extends Panel


onready var leaderPositions = [
	$Entry1, 
	$Entry2,
	$Entry3,
	$Entry4,
	$Entry5,
	$Entry6,
	$Entry7,
	$Entry8,
	$Entry9,
	$Entry10
]

var playerStylebox = load("res://assets/styles/Leaderboard/playerpanel.tres")

var leaderboardResults; 
var pName
var positionID = 0

var leaderStart = 0
var leaderEnd = 0

var numOfEntries = 10

#func _ready():
#	#Hide leaderboard entries incase less than 10 entries per thing
#	for l in leaderPositions:
#		l.hide()
#
#	$LOADING.show() 

#Get the leaderboard information
func init(var songID):
	pName = PlayerInfo.playerName
	Sql.print_leaderboard(songID)
	Sql.connect("leaderboard_ready", self, "displayLeaderboard")


func displayLeaderboard():
	leaderboardResults = Sql.leaderboardResults
	var badgeResults = Sql.badgeResults
	
	print(Sql.badgeResults)
	
	#Sort leaderboard data fpr display	
	leaderboardResults.sort_custom(self, "sortLeaderboard")
	
	for i in range(0, leaderboardResults.size()):
		if leaderboardResults[i].playerName == pName:
			positionID = i
		
	#Handle overflow	
	if leaderboardResults.size() > numOfEntries:
		if positionID >= leaderboardResults.size() - 5 and leaderboardResults.size() > numOfEntries:
			leaderStart = leaderboardResults.size() - numOfEntries
			leaderEnd = leaderboardResults.size()
		elif positionID >= 4:
			leaderStart = positionID - 4
			leaderEnd = positionID + 6
		else:
			leaderStart = 0
			leaderEnd = numOfEntries
	else:
		leaderStart = 0
		leaderEnd = leaderboardResults.size()
		
	var finalResults = []
	for i in range(leaderStart, leaderEnd):
		finalResults.append(leaderboardResults[i])
		
		
	for i in range(0, badgeResults.size()):
		for j in range(0, finalResults.size()):
			if(badgeResults[i].name == finalResults[j].playerName):
				var badgeIndex = badgeResults[i].badge
				if(badgeIndex != -1):
					leaderPositions[j].get_node("badge").texture = ChallengeManager.challengeBadge[badgeIndex]

	for i in range(0, finalResults.size()):
		leaderPositions[i].get_node("position").text = String(leaderStart + i + 1)
		leaderPositions[i].get_node("score").text = String(finalResults[i].score)
		leaderPositions[i].get_node("name").text = finalResults[i].playerName
		
		if finalResults[i].playerName == pName:
			leaderPositions[i].set('custom_styles/panel', playerStylebox)

	#for i in range(0, badgeResults.size()):
		

			
	for i in range(0,finalResults.size()):
		leaderPositions[i].show()
		
	$LOADING.hide() 

func sortLeaderboard(a, b):
	if a.score > b.score:
		return true
	return false

func _on_ReturnToMain_pressed():
	queue_free()
	get_tree().change_scene("res://scenes/ChooseSong/ChooseSong.tscn")
	
