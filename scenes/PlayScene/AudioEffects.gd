extends Node2D

var musicBusIndex = 0
var chorus
var lowPass
var chorusMaxWet = 0.7
var lowPassResonnance = 0.4

func init():
	musicBusIndex = AudioServer.get_bus_index("Music")
	AudioServer.set_bus_effect_enabled(musicBusIndex, 0, false)
	AudioServer.set_bus_effect_enabled(musicBusIndex, 1, false)

	chorus = AudioServer.get_bus_effect(musicBusIndex, 1)
	lowPass = AudioServer.get_bus_effect(musicBusIndex, 0)


func setAudioEffects(progress):
	#Really should do checks first to see if enabled
	if(progress > 35):
		AudioServer.set_bus_effect_enabled(musicBusIndex, 0, false)
		AudioServer.set_bus_effect_enabled(musicBusIndex, 1, false)
		
		lowPass.resonance = 1
		chorus.wet = 0
	else:
		AudioServer.set_bus_effect_enabled(musicBusIndex, 0, true)
		AudioServer.set_bus_effect_enabled(musicBusIndex, 1, true)
		
		var p = progress / 35
		chorus.wet = 1 - (p)
		lowPass.resonance = 0.5 + (p / 2)
