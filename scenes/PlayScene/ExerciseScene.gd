extends Node2D

# Packed scenes for adding to the main scene
export (PackedScene) var Note
export (PackedScene) var Bar

var playing = false

# Variables for storing the active nodes
var curr_bar1 = []
var curr_bar2 = []

var next_bar1 = []
var next_bar2 = []

var notes_hit = 0
var note_nodes = []

# stores bpm
# TODO read this from a beats file 
# TODO make a beats file!
var bpm = 85.0

# stores distance for pointer to move per phys process
# maybe move to function 
var distToTravel = 0

# stores the data loaded from the json file
var music_json
var songID = 0

var songLengthInBars
var lengthOfBar

var beatsPerSecond = 0

var isStart = true

var currentMaxDifficultyValue = 0
var currentMinDifficultyValue = 0 

var notesHitInBar = 0 
var notesInBar = 0
var totalBars = 0

var barNodes = []
var currentBarSwitchDuration = 0
#For call and repeat
var isRepeat = false
var isFirstBar = true
var exerciseType = "practice"
var songJSON
var easyReadMode = false
var description = ""
var restImage = ""
var noteImage = ""
var allStageBarJSON
var exerciseName = ""

var beatsInBar = 4

var tutorialIsPlaying = true

func init(var songJSONIn, bpmPassed, easyRead, desc, restImg, noteImg, allJSON):
	beatsInBar = songJSONIn["beatsPerBar"]
	$NoteNodes.beatsInBar = beatsInBar
	$Failed.hide()
	$GUI.hideProgress()
	songJSON = songJSONIn
	$Scoring.alwaysHitNotes = true
	easyReadMode = easyRead
	bpm = bpmPassed
	songID = songJSONIn["id"]
	var beats = songJSONIn["beats"]
	exerciseName = songJSONIn["name"]
	
	allStageBarJSON = allJSON
	description = desc
	restImage = restImg
	noteImage = noteImg
	
	
	var duration = tutorial(true)
	$Timer.wait_time = float(duration)
	$Timer.start()
	$Tween.interpolate_property($ColorRect,"color",$ColorRect.color,Color('#e1d1cf'),0.25,Tween.TRANS_LINEAR)
	$Tween.start()
	#yield(get_tree().create_timer(duration), "timeout")

		
		
func _on_Timer_timeout():
	print("TIMER IS COUNTED DOWN")
	tutorial(false)
	$GUI.startCountdown(beatsInBar)
	tutorialIsPlaying = false
	
	$Scoring.scoreModifier = float(bpm) / 100

	var newStream = load(str("res://assets/exercises/",String(bpm),"bpm",String(beatsInBar),".mp3"))
	$MusicPlayer.stream = newStream

	barNodes = get_tree().get_nodes_in_group("bars")
	$Pointer.position = Vector2(barNodes[0].position.x+($Bar1.get_width() / 9), barNodes[0].position.y- $Bar1.get_width()/3)
	if(easyReadMode):
		barNodes[2].scale.x = 0.67
		barNodes[3].scale.x = 0.67
	# Parse text as Json
	music_json = GetFiles.getMusicJSON(songJSON["beats"])
	
	next_bar1 = $ChooseBars.returnBar(music_json, currentMaxDifficultyValue)
	next_bar2 = $ChooseBars.returnBar(music_json, currentMaxDifficultyValue)
	
	beatsPerSecond = (bpm/60.0)
	lengthOfBar = (1/beatsPerSecond)*beatsInBar
	currentBarSwitchDuration = lengthOfBar
	# Place notes on the scene w Json
	
	totalBars = ($MusicPlayer.stream.get_length()/(1/beatsPerSecond)) / 4
	$BufferTimer.wait_time=(1/beatsPerSecond)/2
	
	notesInBar = place_notes(music_json)
	
	#Disable the first notes ready for animation to be played
	for note in note_nodes:
		note.hideNote()
	
	#Play animations
	for note in note_nodes:
		yield(get_tree().create_timer(0.1), "timeout")
		note.noteStartAnim()
	
	
	
func tutorial(tutorialIsPlaying):
	var duration = 0
	if(tutorialIsPlaying):
		$Bar1.hide()
		$Bar2.hide()
		$Bar3.hide()
		$Bar4.hide()

		duration = $ExerciseGUI.tutorialIsOn(true)
		$GUI.tutorial(true)
	else:
		$Bar1.show()
		$Bar2.show()
		$Bar3.show()
		$Bar4.show()

		$ExerciseGUI.tutorialIsOn(false)
		$GUI.tutorial(true)		
	
	return duration 
	
#func _input(event):
#	if event.is_action_pressed("click"):
#		if(!isRepeat):
#			return
#
#		$Scoring.noteClicked()


func noteHit(didHit, noteHit):
	if !didHit:
		noteHit.drawMissed()


# Note Placement
# --------------
# Funciton to remove notes from scene
func remove_notes():
	# For each note on screen, remove from the scene
	for note in note_nodes:
		note.queue_free()
		
	note_nodes.clear()
	$Scoring.clearClickedIcons()
	

# Function to place notes on screen
func place_notes(bars):
	#Reset the current note ready for new bar set
	$Scoring.curNote = 0
	
	# - Bar selection -
	# Stores the randomly selected bars
	var selectedBars = []
	
	# Select a random bar 3 times
	curr_bar1 = next_bar1
	curr_bar2 = next_bar2
	
	#Selects the max bar that can be displayed based on difficulty
	
	if(!isRepeat and !isFirstBar):
		next_bar1 = $ChooseBars.returnBar(music_json, currentMaxDifficultyValue)
		next_bar2 = $ChooseBars.returnBar(music_json, currentMaxDifficultyValue)
	else:
		next_bar1 = curr_bar1
		next_bar2 = curr_bar2


		
	selectedBars.append(curr_bar1)
	selectedBars.append(curr_bar2)
	selectedBars.append(next_bar1)
	selectedBars.append(next_bar2)
		
	#Draws the notes to the screen
	note_nodes = $NoteNodes.populateNodes(selectedBars, barNodes, lengthOfBar, $Bar1.get_width())	
	
	if(isRepeat or isFirstBar):
		for note in note_nodes:
			note.setCollisionShapeSmall()
	
	isFirstBar = false
	
	return $NoteNodes.notesInPlayableBars



# Playing the song
# --------------

# Play the notes
func play_notes():
	#print("Beats per minute:",bpm)
	beatsPerSecond = (bpm/60.0)
	#print("Beats per second:",beatsPerSecond)
	
	#print(lengthOfBar)
	distToTravel = (($Bar1.get_width())/lengthOfBar)/300
	songLengthInBars = stepify($MusicPlayer.stream.get_length()/lengthOfBar, 1)
	
	# Start Pointer
	# TODO This current code sets the pointer to the first note of the first bar
	# What would be nice is for the pointer to run a long an initial bar during
	# the countdown 	
	isStart = false
	playing = true
	$MusicPlayer.play()





# Stores which bar the pointer is on
var barCount = 0
var totalBarCount = 0
var switchAnimPlayed = false
# Called every physics process
# Roughly 60 times a second
# This is used instead of process to ensure the frame rate does no interfere 
# with the music timing
func _physics_process(_delta):
	if(tutorialIsPlaying):
		return
	
	if playing:
		# Increase pointer pos 		
		$Pointer.translate(Vector2(distToTravel,0))
		var curPos = $Pointer.position 
		var hasSwitchColour = false
		var musicTime = $MusicPlayer.get_playback_position() + AudioServer.get_time_since_last_mix()
		musicTime -=  AudioServer.get_output_latency()
		
		if(musicTime >= currentBarSwitchDuration - 0.65 and !hasSwitchColour):
			if(barCount == 1):
				if(isRepeat):
					$Tween.interpolate_property($ColorRect,"color",$ColorRect.color,Color('#e1d1cf'),0.25,Tween.TRANS_LINEAR)
					$Tween.start()
				else:
					$Tween.interpolate_property($ColorRect,"color",$ColorRect.color,Color('#cfe1db'),0.25,Tween.TRANS_LINEAR)
					$Tween.start()
			hasSwitchColour = true
			
		# If reached the end of a bar, move to next bar
		# TODO: MAKE THIS RELATIVE TO BAR POSITION + LENGTH
		if(musicTime >= currentBarSwitchDuration):
			currentBarSwitchDuration += lengthOfBar
			totalBarCount +=1
			hasSwitchColour = false
			#print("What the time should be: ", totalBarCount*lengthOfBar)
			#print("Current time: ", $MusicPlayer.get_playback_position())
			#print("Diff: ", (totalBarCount*lengthOfBar) - $MusicPlayer.get_playback_position())
			if totalBarCount < songLengthInBars:
				if barCount == 1:
					barCount = 0
					$Pointer.position = Vector2(barNodes[0].position.x+($Bar1.get_width() / 9), barNodes[0].position.y- $Bar1.get_width()/3)
					remove_notes()
					#Basic dynamic difficulty displaying of bars
					if(notesHitInBar > notesInBar / 2):
						if(currentMaxDifficultyValue <= 0.95):
							currentMaxDifficultyValue += 0.05
					else:
						if(currentMaxDifficultyValue >= 0.1):
							if(currentMaxDifficultyValue >= 0.65):
								currentMaxDifficultyValue -= 0.225
							else:
								currentMaxDifficultyValue -= 0.125
								
					notesHitInBar = 0
					notesInBar = place_notes(music_json)
					switchAnimPlayed = false
					if(isRepeat):
						isRepeat = false
						$Scoring.alwaysHitNotes = true
					else:
						isRepeat = true
						$Scoring.alwaysHitNotes = false
					$Scoring.curNote = 0
					$ExerciseGUI.setTask(isRepeat)
				else:
					$Scoring.curNote = 0
					barCount += 1
					$Pointer.position = Vector2(barNodes[barCount].position.x + ($Bar1.get_width() / 9), (barNodes[barCount].position.y - $Bar1.get_width()/3))
			
				if(totalBarCount > 1 and barCount == 0):
					for note in note_nodes:
						note.noteNewBar()
			
			else:
				end_song()
				
	if Input.is_action_just_pressed("click"):
		if(!isRepeat):
			return
		$Scoring.noteClicked()
#		if curPos.x > (barNodes[barCount].position.x + $Bar1.get_width() - 20) and barCount == 1 and !switchAnimPlayed:
#			$AnimationPlayer.play("Move 1-2")
#			switchAnimPlayed = true
				

# Signal signal to start song
# TODO switch this to when scene is entered
func _on_GUI_start_song():
	$Scoring.canHitNotes = true
	play_notes()

# Sets the speed for the countdown at the start
func _on_GUI_start_countdown():
	$GUI.song_countdown(bpm, isStart)

# Plays metronome sound
func _on_GUI_sound_metronome():
	$MetronomeSound.play()


func end_song():
	playing = false
	$MusicPlayer.stop()
	$Scoring.callResultsScreen("practice", 1)

