extends Node2D

signal clicked_note
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var noteID = 0
var noteType
var restType
var isMiss
var duration
var barIndex = 0
var noteWidthInNoteHeads =0.0

var timeInSong = 0
var timeInBar = 0
var inPlayableBar

var isRest = false
var noteClicked = false
var noteMissed = false

var beamingType = ""

var noteScale = Vector2(1,1)

var twoB = load("res://scenes/NoteBeams/2b.tscn")
var oneB = load("res://scenes/NoteBeams/1b.tscn")
var twoBl = load("res://scenes/NoteBeams/2bl.tscn")
var oneBT = load("res://scenes/NoteBeams/1bt.tscn")
var dot = load("res://scenes/NoteBeams/dot.tscn")
var bt = load("res://scenes/NoteBeams/bt.tscn")
var btt = load("res://scenes/NoteBeams/0bt.tscn")

var missedNote = load("res://scenes/PlayScene/Leftover.tscn")

var isFirstInLine = false
var easyReadMode = false

func init(var note_type, rest, id, isMissed, barTime, durationOfNote, numOfEighth, scaleIn, beamingTypeIn, barI):
	noteType = note_type
	restType = rest
	isMiss = isMissed
	noteID = id
	timeInBar = barTime
	duration = durationOfNote
	noteScale = scaleIn
	beamingType = beamingTypeIn
	barIndex = barI

	

var noteBar
var prevNotePosition
var nextNotePosition

var beamType
var barScaleIncrease 
var shouldDrawBeamNote = false
var beamWidth = 0.0

func drawNote(nextNote, previousNote):	
	prevNotePosition = previousNote
	nextNotePosition = nextNote
	
	barScaleIncrease = 1 - (noteScale.x)
	barScaleIncrease = barScaleIncrease + 1.1
	
	
	if(barIndex == 2 or barIndex == 3):
		barScaleIncrease += 0.63

	beamWidth = $NoteHolder/AnimatedSpriteHolder.global_position.distance_to(nextNote.get_node("NoteHolder/AnimatedSpriteHolder").global_position)
	returnBeamType()

	$NoteHolder/AnimatedSpriteHolder/AnimatedSprite.scale = noteScale
	
	#Draws the note as a rest if its a rest
	if restType:
		$NoteHolder/AnimatedSpriteHolder/AnimatedSprite.animation = str(noteType,"R")
		if("dot" in noteType):
			var d = dot.instance()	
			$NoteHolder/AnimatedSpriteHolder/AnimatedSprite.add_child(d)
		isRest = true
	elif shouldDrawBeamNote:
		$NoteHolder/AnimatedSpriteHolder/AnimatedSprite.animation = str(noteType, "bar")#	
		if("dot" in noteType):
			var d = dot.instance()	
			$NoteHolder/AnimatedSpriteHolder/AnimatedSprite.add_child(d)
		
		if(beamType != null):
			$NoteHolder/AnimatedSpriteHolder/AnimatedSprite.add_child(beamType)
	else:
		if("dot" in noteType):
			var d = dot.instance()	
			$NoteHolder/AnimatedSpriteHolder/AnimatedSprite.add_child(d)
		$NoteHolder/AnimatedSpriteHolder/AnimatedSprite.animation = noteType		
	#positionNotes(false)
	setCollisionShapes(previousNote)	


#Draw a missed note sprite in the position of the current note that has been missed
func drawMissed():
	$NoteHolder/AnimatedSpriteHolder/AnimatedSprite/Missed.show()
	var missed_note = missedNote.instance()
	$"../../".get_node("NoteNodes").missedNotes.append(missed_note)
	
	if($"../../".barCount == 0):
		$"../".call_deferred("add_child", missed_note)
	else:
		$"../".call_deferred("add_child", missed_note)
		
	missed_note.position = position
	missed_note.get_node("NoteHolder/AnimatedSpriteHolder").position = $NoteHolder/AnimatedSpriteHolder.position
	var sprite = missed_note.get_node("NoteHolder/AnimatedSpriteHolder/AnimatedSprite")
	
	sprite.animation = $NoteHolder/AnimatedSpriteHolder/AnimatedSprite.animation
	sprite.scale = $NoteHolder/AnimatedSpriteHolder/AnimatedSprite.scale
	sprite.offset = $NoteHolder/AnimatedSpriteHolder/AnimatedSprite.offset
	
	if("dot" in noteType):
		var d = dot.instance()	
		missed_note.get_node("NoteHolder/AnimatedSpriteHolder/AnimatedSprite").add_child(d)
		
	if(beamType != null):
		missed_note.get_node("NoteHolder/AnimatedSpriteHolder/AnimatedSprite").add_child(beamType)

#THIS WHOLE FUNCTION REALLY NEEDS CLEANING UP 
func returnBeamType():
	match beamingType:
		#No beams at all
		"n":
			shouldDrawBeamNote = false
		#Something beams to it but not from it
		"0b":
			shouldDrawBeamNote = true
		#1 beam to the next note
		"1b":
			shouldDrawBeamNote = true
			beamType = oneB.instance()
			beamType.get_node("TopBeam").rect_size = Vector2(beamWidth, 13)
			beamType.get_node("TopBeam").rect_scale = Vector2(barScaleIncrease, 1.5)
		#2 beams to the next note
		"2b":
			shouldDrawBeamNote = true
			beamType = twoB.instance()
			beamType.get_node("TopBeam").rect_size = Vector2(beamWidth, 13)
			beamType.get_node("TopBeam").rect_scale = Vector2(barScaleIncrease, 1.5)
			beamType.get_node("BottomBeam").rect_size = Vector2(beamWidth, 13)
			beamType.get_node("BottomBeam").rect_scale = Vector2(barScaleIncrease, 1.5)
		#2 beams (second only half way) to the next note
		"2bl":
			shouldDrawBeamNote = true
			beamType = twoBl.instance()	
			beamType.get_node("TopBeam").rect_size = Vector2(beamWidth, 13)
			beamType.get_node("TopBeam").rect_scale = Vector2(barScaleIncrease, 1.5)
		#beams to the next note with a triplet 
		"1bt":
			shouldDrawBeamNote = true
			beamType = oneBT.instance()
			beamType.get_node("TopBeam").rect_size = Vector2(beamWidth, 13)
			beamType.get_node("TopBeam").rect_scale = Vector2(barScaleIncrease, 1.5)
		#Doesn't beam to next note but shows triplet
		"bt":
			shouldDrawBeamNote = true
			beamType = bt.instance()
			beamType.get_node("TopBeam").rect_size.x = beamWidth + 7
			beamType.get_node("TopBeam").rect_scale.x = barScaleIncrease
		#End of triplet
		"btt":
			shouldDrawBeamNote = true
			beamType = btt.instance()
			beamType.get_node("TopBeam").rect_size.x = beamWidth + 7
			beamType.get_node("TopBeam").rect_scale.x = barScaleIncrease


#Position the notes so they're sitting on the stave
func positionNotes(isMissed, previousNote):
	if(!restType):
		$NoteHolder/AnimatedSpriteHolder/AnimatedSprite.offset = (Vector2(0,-45))
	if(!isMissed and !easyReadMode):
		if(!isFirstInLine):
			var previousPosition = previousNote.get_node("NoteHolder/AnimatedSpriteHolder").global_position	
			$NoteHolder/AnimatedSpriteHolder.global_position.x = previousPosition.x + noteWidthInNoteHeads

	
func setStartGlobalPosition(pos):
	$NoteHolder/AnimatedSpriteHolder.global_position.x = pos
	
func returnGlobalPosition():
	return 	$NoteHolder/AnimatedSpriteHolder.global_position.x


# Set the collision shapes for rests
func setCollisionShapes(prevNote):
	#Eighth note collsion boxes need to be smaller so they don't overlap 
	if (($NoteHolder/AnimatedSpriteHolder/AnimatedSprite.animation == "eighth" or 
	$NoteHolder/AnimatedSpriteHolder/AnimatedSprite.animation == "eighthbar") or (
	prevNote.noteType == "eighth" or prevNote.noteType == "eighthbar")) and !isMiss:
		var collisionShape = RectangleShape2D.new()
		collisionShape.extents = Vector2(23,10)
		$NoteHolder/CollisionArea/CollisionShape.shape = collisionShape
		
	if (($NoteHolder/AnimatedSpriteHolder/AnimatedSprite.animation == "sixteenth" or
	$NoteHolder/AnimatedSpriteHolder/AnimatedSprite.animation == "quarterTriplet" or 
	$NoteHolder/AnimatedSpriteHolder/AnimatedSprite.animation == "quarterTripletbar") or (prevNote.noteType == "sixteenth" 
	or prevNote.noteType == "quarterTriplet" or prevNote.noteType == "quarterTripletbar")) and !isMiss:
		var collisionShape = RectangleShape2D.new()
		collisionShape.extents = Vector2(12,10)
		$NoteHolder/CollisionArea/CollisionShape.shape = collisionShape
		
	if "R" in $NoteHolder/AnimatedSpriteHolder/AnimatedSprite.animation and !isMiss:
		var collisionShape = RectangleShape2D.new()
		collisionShape.extents = Vector2(0.5,10)
		$NoteHolder/CollisionArea/CollisionShape.shape = collisionShape



func setCollisionShapeSmall():
	var collisionShape = RectangleShape2D.new()
	collisionShape.extents = Vector2(0.5,10)
	$NoteHolder/CollisionArea/CollisionShape.shape = collisionShape


#Checks to see if the mouse is in the collison object when clicked
func _on_CollisionArea_input_event(viewport, event, shape_idx):
	if event.is_action_pressed("click"):
		noteClicked = true
		emit_signal("clicked_note", noteID) 


func hideNote():
	$NoteHolder/AnimatedSpriteHolder/AnimatedSprite.hide()


func animateNote():
	$NoteHolder.get_node("AnimatedSpriteHolder/AnimatedSprite").modulate = Color(0,1,0, 0.6)		
	$NoteHolder.get_node("AnimationPlayer").play(returnAnimation())
	if($NoteHolder/AnimatedSpriteHolder/AnimatedSprite.animation == "quarterR"):
		$NoteHolder/AnimatedSpriteHolder/quarterR.emitting = true
	elif($NoteHolder/AnimatedSpriteHolder/AnimatedSprite.animation == "halfR" or $NoteHolder/AnimatedSpriteHolder/AnimatedSprite.animation == "halfdot"):
		$NoteHolder/AnimatedSpriteHolder/halfR.emitting = true
	elif($NoteHolder/AnimatedSpriteHolder/AnimatedSprite.animation == "eighthR" or $NoteHolder/AnimatedSpriteHolder/AnimatedSprite.animation == "sixteenthR"):
		$NoteHolder/AnimatedSpriteHolder/quarterR.emitting = true
	else:
		$NoteHolder/AnimatedSpriteHolder/NoteParticle.emitting = true
	
func noteStartAnim():
	$NoteHolder.get_node("AnimationPlayer").play("Intro")
	yield(get_tree().create_timer(0.01), "timeout")
	$NoteHolder/AnimatedSpriteHolder/AnimatedSprite.show()
	
func noteNewBar():
	$NoteHolder.get_node("AnimationPlayer").play("LoadIn")

func gameOver():
	
	if noteMissed:
		return
	
	noteMissed = true
	#$NoteHolder.sleeping = false
	var rng = RandomNumberGenerator.new()
	
	$NoteHolder.gravity_scale = 20
	var left = randi()%2
	
	rng.randomize()
	var yForce = rng.randf_range(300, 600)
	rng.randomize()
	var xForce = rng.randf_range(30, 60)
	rng.randomize()
	var angVel = rng.randf_range(0.3, 0.6)
	
	if left == 1:
		xForce = -xForce
		angVel = -angVel
	
	
	var force = Vector2(xForce,-yForce)
	$NoteHolder.apply_central_impulse(force)
	$NoteHolder.angular_velocity = angVel


	$NoteHolder.get_node("AnimationPlayer").play("Fail")
	
	
func returnAnimation():
	var animToPlay = "NoteHit"
	match(randi()%4+1):
		2:
			animToPlay = "NoteHit (copy)"
		3:
			animToPlay = "NoteHit (copy) (copy)"
		4: 
			animToPlay = "NoteHit (copy) (copy) (copy)"

	return animToPlay
