extends PanelContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.



func _on_PopupExit_pressed():
	#get_tree().paused = false
	get_tree().get_root().get_node("PlayScene/GUI").metronomePaused()
	hide()


func _on_CheckBox_toggled(button_pressed):
	if(button_pressed):
		get_tree().get_root().get_node("PlayScene/Pointer").show()
		get_tree().get_root().get_node("PlayScene").get_node("Scoring").scoreModifier -= 0.4
	else:
		get_tree().get_root().get_node("PlayScene/Pointer").hide()
		get_tree().get_root().get_node("PlayScene").get_node("Scoring").scoreModifier += 0.4


func _on_HitIconsCheck_toggled(button_pressed):
	if(button_pressed):
		get_tree().get_root().get_node("PlayScene").get_node("Scoring").drawClickedIcons = true
		get_tree().get_root().get_node("PlayScene").get_node("Scoring").scoreModifier -= 0.2
	else:
		get_tree().get_root().get_node("PlayScene").get_node("Scoring").clearClickedIcons()
		get_tree().get_root().get_node("PlayScene").get_node("Scoring").drawClickedIcons = false
		get_tree().get_root().get_node("PlayScene").get_node("Scoring").scoreModifier += 0.2
