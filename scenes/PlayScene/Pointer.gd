extends Node2D

# Singals to tell main loop when pointer enters/leaves notes
signal Pointer_area_entered
signal note_entered
signal note_exited

var noteIsHit
var currentNote

# Triggered when pointer enters another area
func _on_Area2D_area_entered(area):
	#print("Entered note")
	emit_signal("note_entered")
	currentNote = area
	
# Triggered when pointer leaves another area
func _on_Area2D_area_exited(area):
	#var noteFound = area.get_parent()
	var noteFound = area.get_node("../../")

	if !noteFound.isRest:	
		emit_signal("note_exited")
	else:
		emit_signal("note_exited")

func getNote():
	if(!is_instance_valid(currentNote)):
		return null
		
	return currentNote.get_node("../../")

