extends Node2D

# Packed scenes for adding to the main scene
export (PackedScene) var Note
export (PackedScene) var Bar

var playing = false

# Variables for storing the active nodes
var curr_bar1 = []
var curr_bar2 = []

var next_bar1 = []
var next_bar2 = []

var notes_hit = 0
var note_nodes = []

# stores bpm
# TODO read this from a beats file 
# TODO make a beats file!
var bpm = 85.0
var exerciseType = "song"
# stores distance for pointer to move per phys process
# maybe move to function 
var distToTravel = 0

# stores the data loaded from the json file
var music_json
var songID = 0

var songLengthInBars
var lengthOfBar

var beatsPerSecond = 0
var songFailed = false

var stage = 1

var isStart = true

var currentMaxDifficultyValue = 0
var currentMinDifficultyValue = 0 

var notesHitInBar = 0 
var notesInBar = 0
var totalBars = 0

var barNodes = []
var currentBarSwitchDuration = 0 

var songJSON
var allStageBarJSON
var easyReadMode = false

var notesInBar1 = 0
var notesInBar2 = 0

var bar1ID = 0
var bar2ID = 0

var beatsInBar = 4

var bgColours = ['#dcdcdc', '#cae9d6', '#cae9e3', '#cadce9', '#e4cae9', '#e9cad4', '#e9caca', '#dfb0b0', '#e69d9d', '#e1cfd8']



func init(var songJSONIn, easyRead, allJSON):
	bpm = songJSONIn["bpm"]
	songID = songJSONIn["id"]
	beatsInBar = songJSONIn["beatsPerBar"]
	$NoteNodes.beatsInBar = beatsInBar
	
	$GUI.startCountdown(beatsInBar)
	$Failed.hide()
	$GUI/ProgressBar.value = $Progress.progress
	easyReadMode = easyRead
	songJSON = songJSONIn
	allStageBarJSON = allJSON
	
	$ChooseBars.setBars(allStageBarJSON)
	

	var beats = songJSONIn["beats"]

	var newStream = load(str("res://assets/music/",songJSONIn["file_name"]))
	$MusicPlayer.stream = newStream
	$AudioEffects.init()

	barNodes = get_tree().get_nodes_in_group("bars")
	$Pointer.position = Vector2(barNodes[0].position.x+($Bar1.get_width() / 9), barNodes[0].position.y- $Bar1.get_width()/3)
	
	if(easyReadMode):
		barNodes[2].scale.x = 0.67
		barNodes[3].scale.x = 0.67
	
	# Parse text as Json
	music_json = GetFiles.getMusicJSON("randomBeats2.json")
	$ChooseBars.utilities = $ChooseBars.computeUtility()
	next_bar1 = $ChooseBars.returnBar(music_json, currentMaxDifficultyValue)
	next_bar2 = $ChooseBars.returnBar(music_json, currentMaxDifficultyValue)
	
	beatsPerSecond = (bpm/60.0)
	lengthOfBar = (1/beatsPerSecond)*beatsInBar
	currentBarSwitchDuration = lengthOfBar
	# Place notes on the scene w Json
	
	totalBars = ($MusicPlayer.stream.get_length()/(1/beatsPerSecond)) / 4
	$BufferTimer.wait_time=(1/beatsPerSecond)/2
	
	notesInBar = place_notes(music_json)
	
	#Disable the first notes ready for animation to be played
	for note in note_nodes:
		note.hideNote()
	
	#Play animations
	for note in note_nodes:
		yield(get_tree().create_timer(0.1), "timeout")
		note.noteStartAnim()
	
	
func _input(event):
	if(songFailed):
		return 
	
#	if event.is_action_pressed("click"):
#		$Scoring.noteClicked()

var setupStageUI = false
var hasSetupStageUI = false
func noteHit(didHit, noteHit):
	if didHit:
		$Progress.progressUp(noteHit.noteType)
		$AudioEffects.setAudioEffects($Progress.progress)
		setupStageUI = $Progress.shouldIncreaseLevel
		$HitAnims.seek(0)
		$HitAnims.play("HitAnim")
		if(setupStageUI and !hasSetupStageUI):
			setupStageUI = false
			hasSetupStageUI = true
			$GUI.prepStage()
			$LevelUpWarning.play()
	else:
		$Progress.progressDown(noteHit.noteType)
		noteHit.drawMissed()
		$AudioEffects.setAudioEffects($Progress.progress)

func stageIncrease():
	if(stage <= 10):
		var c = Color(bgColours[stage])
		$GUI.stageIncrease(stage)
		$Progress.checkStageIncrease()
		$LevelUpSound.play()
		setupStageUI = false
		hasSetupStageUI = false
		$Tween.interpolate_property($ColorRect,"color",$ColorRect.color,c,2,Tween.TRANS_LINEAR)
		$Tween.start()
	

# Note Placement
# --------------
# Funciton to remove notes from scene
func remove_notes():
	# For each note on screen, remove from the scene
	for note in note_nodes:
		note.queue_free()
		
	note_nodes.clear()
	$Scoring.clearClickedIcons()
	

# Function to place notes on screen
func place_notes(bars):
	# TODO placement is a bit weird atm, maybe refactor it
	if songFailed:
		return
		
	#Reset the current note ready for new bar set
	$Bar3.position.x = 350.56
	$Bar4.position.x = 637.726
	# - Bar selection -
	# Stores the randomly selected bars
	var selectedBars = []
	
	# Select a random bar 3 times
	curr_bar1 = next_bar1
	curr_bar2 = next_bar2
	
	notesInBar1 = curr_bar1["notes"].size()
	notesInBar2 = curr_bar2["notes"].size()
	
	bar1ID = curr_bar1["id"]
	bar2ID = curr_bar2["id"]
	#NEED TO GET THE RIGHT NUMBER OF NOTES, ALSO NEED TO GET ID OF BAR

	
	#Selects the max bar that can be displayed based on difficulty


	next_bar1 = $ChooseBars.returnBar(music_json, currentMaxDifficultyValue)
	next_bar2 = $ChooseBars.returnBar(music_json, currentMaxDifficultyValue)

	if(totalBarCount != 0):
		$ChooseBars.utilities = $ChooseBars.computeUtility()
	
	selectedBars.append(curr_bar1)
	selectedBars.append(curr_bar2)
	selectedBars.append(next_bar1)
	selectedBars.append(next_bar2)
		
	#Draws the notes to the screen
	note_nodes = $NoteNodes.populateNodes(selectedBars, barNodes, lengthOfBar, $Bar1.get_width())
	return $NoteNodes.notesInPlayableBars



# Playing the song
# --------------

# Play the notes
func play_notes():
	if songFailed:
		return
	
	#print("Beats per minute:",bpm)
	beatsPerSecond = (bpm/60.0)
	#print("Beats per second:",beatsPerSecond)
	
	#print(lengthOfBar)
	distToTravel = (($Bar1.get_width())/lengthOfBar)/300
	songLengthInBars = stepify($MusicPlayer.stream.get_length()/lengthOfBar, 1)
	
	# Start Pointer
	# TODO This current code sets the pointer to the first note of the first bar
	# What would be nice is for the pointer to run a long an initial bar during
	# the countdown 	
	isStart = false
	playing = true
	$MusicPlayer.play()





# Stores which bar the pointer is on
var barCount = 0
var totalBarCount = 0
var switchAnimPlayed = false
# Called every physics process
# Roughly 60 times a second
# This is used instead of process to ensure the frame rate does no interfere 
# with the music timing
var shouldStartNextLevel = false
var hasLooped = false



var timer = 0
func _physics_process(_delta):
	
	if songFailed:
		return 

	if Input.is_action_just_pressed("click"):
		$Scoring.noteClicked()

	if playing:
		#Calculating points
		timer += _delta
		if(timer >= lengthOfBar / 4):
			$Scoring.incrementPoints()
			timer = 0
		
		# Increase pointer pos 		
		$Pointer.translate(Vector2(distToTravel,0))
		var curPos = $Pointer.position 
		
		var musicTime = $MusicPlayer.get_playback_position() + AudioServer.get_time_since_last_mix()
		musicTime -=  AudioServer.get_output_latency()

		
		if(musicTime >= currentBarSwitchDuration):
			totalBarCount += 1
			currentBarSwitchDuration += lengthOfBar
					
			if barCount == 1:
				barCount = 0
				switchAnimPlayed = false
				$Camera2D.position.x = 620
				$Pointer.position = Vector2(barNodes[0].position.x + ($Bar1.get_width() / 9), barNodes[0].position.y - $Bar1.get_width()/3)
				remove_notes()
				$Progress.setBaseProgress()
				
				if(shouldStartNextLevel):
					shouldStartNextLevel = false
					stageIncrease()
					
				shouldStartNextLevel = $Progress.shouldIncreaseLevel
				if(shouldStartNextLevel and stage <= 8):
					stage += 1
					$ChooseBars.stageIncrease(stage)
			
				var percentHit =  float(notesHitInBar) / float(notesInBar2)
				var index = Bayes.rhythmPlayed(bar2ID, percentHit)
				
				###########   DEBUG STUFF FOR MEAN AND STDEV ###############
				var stdev = Bayes.loadedResults[index][3]
				var mean = Bayes.loadedResults[index][2]
				
		#		print("THIS IS THE MEAN, ", mean)
			#	print("THIS IS THE STDEV, ", stdev)
				
				$STD.text = ("Stddev: " + str(stdev))
				$MeanStd/Mean.text = ("MEAN: " + str(mean))
				
				var meanStdBarPos = float(300) * float(mean - float(stdev))
				$MeanStd/Fill.rect_position  = Vector2(meanStdBarPos, 0)
				$MeanStd/Fill.rect_size = Vector2(((stdev * 2) * 300), 40)
		
				stdev = Bayes.skill[3]
				mean = Bayes.skill[2]
				
		#		print("THIS IS THE MEAN, ", mean)
			#	print("THIS IS THE STDEV, ", stdev)
				
				$STDSkill.text = ("Stddev: " + str(stdev))
				$MeanStdSkill/MeanSkill.text = ("MEAN: " + str(mean))
				
				meanStdBarPos = float(300) * float(mean - float(stdev))
				$MeanStdSkill/Fill.rect_position  = Vector2(meanStdBarPos, 0)
				$MeanStdSkill/Fill.rect_size = Vector2(((stdev * 2) * 300), 40)
				
				stdev = Bayes.skillResults[index][3]
				mean = Bayes.skillResults[index][2]
				
		#		print("THIS IS THE MEAN, ", mean)
			#	print("THIS IS THE STDEV, ", stdev)
				
				$STDSkillRes.text = ("Stddev: " + str(stdev))
				$MeanStdSkillRes/MeanSkillRes.text = ("MEAN: " + str(mean))
				
				meanStdBarPos = float(300) * float(mean - float(stdev))
				$MeanStdSkillRes/Fill.rect_position  = Vector2(meanStdBarPos, 0)
				$MeanStdSkillRes/Fill.rect_size = Vector2(((stdev * 2) * 300), 40)
				#############################################################
					
				notesHitInBar = 0
				$Scoring.curNote = 0
				notesInBar = place_notes(music_json)

			else:
				var percentHit = float(notesHitInBar) / float(notesInBar1)
				var index = Bayes.rhythmPlayed(bar1ID, percentHit)
				
				###########   DEBUG STUFF FOR MEAN AND STDEV ###############
				var stdev = Bayes.loadedResults[index][3]
				var mean = Bayes.loadedResults[index][2]
				
	#			print("THIS IS THE MEAN, ", mean)
		#		print("THIS IS THE STDEV, ", stdev)
				
				$STD.text = ("Stddev: " + str(stdev))
				$MeanStd/Mean.text = ("MEAN: " + str(mean))
				
				var meanStdBarPos = float(300) * float(mean - float(stdev))
				$MeanStd/Fill.rect_position  = Vector2(meanStdBarPos, 0)
				$MeanStd/Fill.rect_size = Vector2(((stdev * 2) * 300), 40)
		
				#############################################################
				notesHitInBar = 0
				barCount += 1
				$Scoring.curNote = 0
				$Pointer.position = Vector2(barNodes[barCount].position.x + ($Bar1.get_width() / 9), (barNodes[barCount].position.y - $Bar1.get_width()/3))
			
			if(totalBarCount > 1 and barCount == 0):
				for note in note_nodes:
					note.noteNewBar()
		
		if(musicTime < (lengthOfBar - 0.5)):
			currentBarSwitchDuration = lengthOfBar	
			

		var camDist = Vector2((40/lengthOfBar)/300, 0)
		$Camera2D.translate(camDist)
		$Bar3.translate(camDist)
		$Bar4.translate(camDist)
#		if(totalBarCount >= songLengthInBars):
#			songLengthInBars += 2
#			hasLooped = true
#			currentBarSwitchDuration = lengthOfBar
#
#		if(hasLooped and musicTime < currentBarSwitchDuration):
#			hasLooped = false 

#		if(hasLooped):
#			print(musicTime, " music time")
#			print(currentBarSwitchDuration, " cur bar")
	#	if curPos.x > (barNodes[barCount].position.x + $Bar1.get_width()) and barCount == 1 and !switchAnimPlayed:
			#$AnimationPlayer.play("Move 1-2")
			#switchAnimPlayed = true


# Signal signal to start song
# TODO switch this to when scene is entered
func _on_GUI_start_song():
	$Scoring.canHitNotes = true
	play_notes()

# Sets the speed for the countdown at the start
func _on_GUI_start_countdown():
	$GUI.song_countdown(bpm, isStart)

# Plays metronome sound
func _on_GUI_sound_metronome():
	$MetronomeSound.play()


func end_song():
	playing = false
	$MusicPlayer.stop()
	$Scoring.callResultsScreen("song", stage)


func failed_song():
	$MusicPlayer.stop()
	$SongFail.play()
	songFailed = true
	$Scoring.songFailed = true
	$Scoring.clearClickedIcons()
	
	for note in note_nodes:
		note.gameOver()
	
	$GUI.gameOver()
	$Failed.show()
	$AnimationPlayer.play("Failed")
	
	yield(get_tree().create_timer(5), "timeout")
	$Scoring.callResultsScreen("song", stage)



