extends Node2D

export (PackedScene) var Note

var note_nodes = []
var eighthNotesInRow = 1
var currentTimeNotes = 0

var notesInPlayableBars = 0

var missedNote = load("res://scenes/PlayScene/Leftover.tscn")
var missedNotes = []

var noteHeadWidth = 18
var beatsInBar = 4

func populateNodes(selectedBars, barNodes, lengthOfBar, barWidth):
	
	for note in missedNotes:
		note.queue_free()
	
	missedNotes.clear()
	
	var posx = 0
	var posy = 0
	var index = 0
	var barIndex = 0
	var totalTime = 0
	var requiredBarWidth = 0
	var initialBarWidth = 0
	var secondBarStartIndex = 0
	var thirdBarStartIndex = 0 
	var scale = Vector2(1,1)
	notesInPlayableBars = 0
	var totalNotesInBar12 = 0
	var totalNotesInBar34 = 0
	
	for i in range(0, selectedBars.size()):
		if(i == 0 or i == 1):
			totalNotesInBar12 += selectedBars[i]["notes"].size()
		else:
			totalNotesInBar34 += selectedBars[i]["notes"].size()
	
	
	for bar in selectedBars:
		var barTime = 0

		#Gourlay note spacings
		var minDurationOfSelectedBars = getMinimumValue(bar["notes"])
		var eighth = 0.125
		var minDuration = min(minDurationOfSelectedBars, eighth)
		var k = 2 - logWithBase(minDuration, 2)
		var noteWidthsInHeads = 0

		
		#Scales note sizes based on how many notes are in the bar
		#Scales based on whether showing in top or bottom bar
		if(barIndex == 0):
			if(totalNotesInBar12 >= 12):
				noteHeadWidth = 18
				scale = Vector2(0.7, 0.8)
			else:
				noteHeadWidth = 30
				scale = Vector2(0.8, 0.8)
		else:
			if(totalNotesInBar34 >= 12):
				noteHeadWidth = 10
				scale = Vector2(0.4, 0.45)
			else:
				noteHeadWidth = 14
				scale = Vector2(0.5, 0.45)


		for note in bar["notes"]:
			# stores the time value of the note
			var time = note["time"]
			var note_name = note["name"]
			var beamingType = note["beaming"]
			
			var rest = false
			
			if note_name == "r":
				rest = true

			# stores the type of note
			var note_type
			
			# Selecting note type, could do with some cleanup 
			match time:
				0.25:
					note_type = "quarter"
				0.375:
					note_type = "quarterdot"
				0.125:
					note_type = "eighth"
				0.33:
					note_type = "half"
				0.34:
					note_type = "half"
				0.08325:
					note_type = "quarterTriplet"
				0.0835:
					note_type = "quarterTriplet"
				0.1665:
					note_type = "quarter"
				0.167:
					note_type = "quarter"
				0.1875:
					note_type = "eighthdot"
				0.0625:
					note_type = "sixteenth"
				0.5: 
					note_type = "half"
				0.75: 
					note_type = "halfdot"
				1:
					note_type = "whole"
			
		
			if(beatsInBar != 4):
				var timeSig = 1 / beatsInBar
				var timeMultiplier = timeSig / 0.25
				time = time * timeMultiplier
			
			# Create note instance
			index += 1
			var note_node = Note.instance()
			note_node.init(note_type, rest, index, false, barTime, time, eighthNotesInRow, scale, beamingType, barIndex)
			note_nodes.append(note_node)
								
			barTime += time
			
			# If the total duration of the notes in the bar more than one
			if totalTime >= 1:
				barIndex+=1
				noteWidthsInHeads = 5 * noteHeadWidth
				# TODO maybe make a variable or a way to pass this?
				# Increment y position to match up with bars
				posy = 0
				
				if(barIndex == 1):
					secondBarStartIndex = index - 1
				
				#Resets the note widths ready for the start of the next bar
				if(barIndex == 2):
					noteWidthsInHeads = 0
					note_node.isFirstInLine = true
					thirdBarStartIndex = index - 1
				
				# Reset x position
				posx = 0 

				# Reset time
				totalTime = 0
				
			if(barIndex == 0 or barIndex == 2):
				initialBarWidth = requiredBarWidth
				
			#Calculates the distance in pixels that should be between each note
			note_node.noteWidthInNoteHeads = noteWidthsInHeads
			noteWidthsInHeads = ((logWithBase(time, 2) + k) * noteHeadWidth)
			requiredBarWidth += noteWidthsInHeads

	
				
			if barIndex <= 1:	 	
				note_node.timeInSong = currentTimeNotes
				barNodes[barIndex].add_child(note_node)
				var currentNoteLength = (lengthOfBar*time)
				currentTimeNotes += currentNoteLength
				notesInPlayableBars += 1
			else:
				note_node.timeInSong = 0
				note_node.get_node("NoteHolder/AnimatedSpriteHolder/AnimatedSprite").modulate = Color(0,1,0, 0.7)
				#note_node.get_node("NoteHolder").get_node("AnimatedSpriteHolder").get_node("AnimatedSprite").scale = note_node.get_node("NoteHolder").get_node("AnimatedSpriteHolder").get_node("AnimatedSprite").scale * 0.6		
				barNodes[barIndex].add_child(note_node)
				
			note_node.easyReadMode = $"../".easyReadMode 
			
			# TODO MAYBE PUT THIS IN A FUNCTION?
			# Increment the bar's total time with the note's length
			totalTime += time
			
			# Set placement of note
			var placementX = posx + barWidth/9
			note_node.position = Vector2(placementX,posy)
			posx += barWidth*time
		
		#Sets the start position for the first note in the first bar
		if(!$"../".easyReadMode):
			if(barIndex == 1):
				var noteStart = (640) - (requiredBarWidth / 2)
				note_nodes[0].setStartGlobalPosition(noteStart)
				
				var distanceBetweenBarNotes = note_nodes[secondBarStartIndex - 1].get_node("NoteHolder/AnimatedSpriteHolder").global_position.distance_to(note_nodes[secondBarStartIndex].get_node("NoteHolder/AnimatedSpriteHolder").global_position)
				
				$"../".barNodes[0].get_node("BarTexture").rect_size = Vector2(initialBarWidth + (noteHeadWidth * 3) + (noteHeadWidth), 75)
				$"../".barNodes[0].get_node("BarTexture").rect_global_position.x = note_nodes[0].returnGlobalPosition() - (noteHeadWidth)
				
				$"../".barNodes[1].get_node("BarTexture").rect_global_position.x = $"../".barNodes[0].get_node("BarTexture").rect_global_position.x + initialBarWidth + (noteHeadWidth * 3) + (noteHeadWidth)
				$"../".barNodes[1].get_node("BarTexture").rect_size = Vector2(requiredBarWidth - (initialBarWidth) - noteHeadWidth, 75)
				
				requiredBarWidth = 0
				initialBarWidth = 0
				
				
			if(barIndex == 3):
				var noteStart = (640) - (requiredBarWidth / 2)
				note_nodes[thirdBarStartIndex].setStartGlobalPosition(noteStart)
				
				$"../".barNodes[2].get_node("BarTexture").rect_size = Vector2(initialBarWidth + (noteHeadWidth * 3) + (noteHeadWidth), 75)
				$"../".barNodes[2].get_node("BarTexture").rect_global_position.x = note_nodes[thirdBarStartIndex].returnGlobalPosition() - (noteHeadWidth)

				$"../".barNodes[3].get_node("BarTexture").rect_global_position.x = $"../".barNodes[2].get_node("BarTexture").rect_global_position.x + initialBarWidth + (noteHeadWidth * 3) + (noteHeadWidth)
				$"../".barNodes[3].get_node("BarTexture").rect_size = Vector2(requiredBarWidth - (initialBarWidth) - noteHeadWidth, 75)
				
				requiredBarWidth = 0
				initialBarWidth = 0
			
	barIndex = 0
	
	for i in range(0, note_nodes.size()):
		if(i == 0):
			note_nodes[i].positionNotes(false, note_nodes[i])
		else:
			note_nodes[i].positionNotes(false, note_nodes[i-1])
	

	for i in range(0, note_nodes.size()):
		if i == 0:
			note_nodes[i].drawNote(note_nodes[i+1], note_nodes[i])
		elif(i < note_nodes.size() - 1):
			note_nodes[i].drawNote(note_nodes[i+1], note_nodes[i-1])
		else:
			note_nodes[i].drawNote(note_nodes[i-1], note_nodes[i - 1])
	
	return note_nodes


func getMinimumValue(n):
	var minVal = 1
	for i in range(0, n.size()):
		if(n[i]["time"] < minVal):
			minVal = n[i]["time"]
	
	return minVal 

func logWithBase(val, base):
	return log(val) / log(base)
