extends Node2D

var currentMaxDifficultyValue = 0
var currentMinDifficultyValue = 0 

var targetPercentage = 0.81

var stageBars
var currentMusicJSON
var utilities = []

func setBars(allBars):
	stageBars = allBars
	currentMusicJSON = GetFiles.getMusicJSON(stageBars[0]["beats"])

func stageIncrease(stage):
	targetPercentage -= (0.06 + (($"../".stage / 100) / 2))
		
func returnBar(music_json, difficultyVal):
	currentMaxDifficultyValue = difficultyVal
	
	var curMax = 0
	var barIndexToSelect = 0 
	var utilIndex = 0
	for x in range(0, utilities.size()):
		if(utilities[x] > curMax):
			curMax = utilities[x]
			utilIndex = x
			barIndexToSelect = int(Bayes.loadedIDs[x])
		
	# Set utility to 0 for biggest value so next bar doesn't make the same choice
	utilities[utilIndex] = 0
	var bar = []
	
	#ID's of rhythms aren't necessarily going to match up to positions in array so need to make sure we get the right rhythm
	for i in range(0, music_json["RandomBars"].size()):
		if(barIndexToSelect == int(music_json["RandomBars"][i]["id"])):
			barIndexToSelect = i
			break

	bar = music_json["RandomBars"][barIndexToSelect]
	return bar



func computeUtility():
	var out = [] 
	#Modifies the target percentage by between -0.2 (if performing badly) and +0.2 if performing well.
	var random = RandomNumberGenerator.new()
	random.randomize()
	var target = targetPercentage + random.randf_range(-0.05, 0.05)
	
	Bayes.returnFusedSkillandEstimate()
	var performanceFactor = ((float($"../Progress".progress) - float(50)) / float(500))
	print("perf factor", performanceFactor)
	var finalTarget = target - (performanceFactor)
	$"../TargetMean".text = ("Trg Mean: " + str(finalTarget))
	for i in range(Bayes.skillResults.size()):
		var addition = float(1) - (abs(float(finalTarget) - float(Bayes.skillResults[i][2])))
		out.append(Bayes.skillResults[i][3] + addition)

	return out
	


