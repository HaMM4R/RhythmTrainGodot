extends Node2D

var progress = 45
var baseProgressIncrease = 1
var baseProgressDecrease = 0.75
var baseProgressIncreaseMultiplier = 1.0
var baseProgressDecreaseMultiplier = 1.0

var shouldIncreaseLevel = false

func setBaseProgress():
	if(baseProgressIncrease < 25):
		baseProgressIncrease += 0.1
	if(baseProgressIncrease < 28):
		baseProgressDecrease += 0.15

func progressUp(noteType):
	var progressIncrease = baseProgressIncreaseMultiplier
	if(noteType == "half" and $"../".stage == 1):
		progressIncrease *= 1.5
	if(noteType == "quarter"):
		progressIncrease *= 0.5
	if(noteType == "eighth"):
		progressIncrease *= 0.25
	if(noteType == "sixteenth"):
		progressIncrease *= 0.125
		
	var stageMultiplier = float(1) - float((float($"../".stage) / 10))
	var currentProgressMultiplier = (float(progressIncrease) * (1 + (float(100 - progress)) / 100.0)) * stageMultiplier
	progress += ((baseProgressIncrease * currentProgressMultiplier))
	
	print("stage Multiplier", stageMultiplier)
	print("curProgMult, ", currentProgressMultiplier)
	
	if progress > 100:
		shouldIncreaseLevel = true
		
	$"../GUI/ProgressBar".value = progress#50*(1+diff)
	
func progressDown(noteType):
	if($"../".shouldStartNextLevel):
		return
	
	var progressReduce = baseProgressDecreaseMultiplier
	if(noteType == "half" and $"../".stage == 1):
		progressReduce *= 1.5
	if(noteType == "quarter"):
		progressReduce *= 0.5
	if(noteType == "eighth"):
		progressReduce *= 0.25
	if(noteType == "sixteenth"):
		progressReduce *= 0.125
		
	var stageMultiplier = float(1) - (float((float($"../".stage) / 10)) / 2)	
	var currentProgressMultiplier = (float(progressReduce) * (1 + (float(progress) / 100.0))) * stageMultiplier
	progress -= (baseProgressDecrease * currentProgressMultiplier)
	
	if progress <= 0:
		$"../".failed_song()
		
	$"../GUI/ProgressBar".value = progress


func checkStageIncrease():
	if(shouldIncreaseLevel):
		shouldIncreaseLevel = false
		progress = 45
		$"../".stageIncrease()
