extends Node

var playerName = ""
var playerID = 0
var leaderboardGroup = 0
var totalStars = 0
var totalPreviousScore = 0
var currentLevel = 0
var activeSave = ""
var activeDifficulties = ""
var playerSkillMultiplier = 0.0

#This needs to be split into JSON
var xpForLevel = [0, 1500, 2500, 4000, 6000, 8500, 12500, 18000, 25000, 32500]

var levelBadges = [
	load("res://assets/images/rests/Rest semiquaver.png"),
	load("res://assets/images/notes/Semiquaver.png"),
	load("res://assets/images/rests/Rest quaver.png"),
	load("res://assets/images/notes/Quaver single.png"),
	load("res://assets/images/rests/Rest crotchet.png"),
	load("res://assets/images/notes/Crotchet.png"),
	load("res://assets/images/rests/Rest minim.png"),
	load("res://assets/images/notes/Minim.png"),
	load("res://assets/images/rests/Rest semibreve.png"),
	load("res://assets/images/notes/Semibreve.png")
]
