extends CanvasLayer


func setTask(isRepeating):
	if(!isRepeating):
		$TaskLabel.text = "WATCH AND LEARN"
	else:
		$TaskLabel.text = "REPEAT"

func tutorialIsOn(tutorialPlaying):
	var duration = 0 
	
	$ExerciseHelp/Note.texture = load($"../".noteImage)
	$ExerciseHelp/Rest.texture = load($"../".restImage)
	$AudioStreamPlayer.stream = load(str("res://assets/sounds/Tutorials/",String($"../".exerciseName),".mp3"))
	
	if(tutorialPlaying):
		$AudioStreamPlayer.play()
		duration = $AudioStreamPlayer.stream.get_length()
		$AnimationPlayer.play($"../".exerciseName)
		$ExerciseTitle.text = $"../".exerciseName
		$ColorRect.show()
		$SkipButton.show()
		$TaskLabel.hide()
		$ExerciseHelp.show()
		$ExerciseTitle.show()
	else:
		$ColorRect.hide()
		$SkipButton.hide()
		$AudioStreamPlayer.stop()
		$AnimationPlayer.stop()
		$TaskLabel.show()
		$ExerciseHelp.hide()
		$ExerciseTitle.hide()

	return duration


func _on_SkipButton_pressed():
	tutorialIsOn(false)
	$"../Timer".wait_time = 1
	$"../Timer".start()
