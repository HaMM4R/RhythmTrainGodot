extends CanvasLayer

signal start_song
signal start_countdown
signal sound_metronome
# Variable for Countdown timer
var CountdownTimer
# there's probably a better way to do the timer
var TimerCount = 4
var beatsInPar = 4

# Called when the node enters the scene tree for the first time.
func startCountdown(beatsInBarIn):#
	beatsInPar = beatsInBarIn
	TimerCount = beatsInPar
	$PauseMenu/CheckQuit.hide()
	$PauseMenu.hide()
	$PauseButton.hide()
	$CountdownLabel.hide()
	$StreakModButton.hide()
	$Options.hide()
	$ScoreLabel.show()
	$StreakLabel.show()
	yield(get_tree().create_timer(1), "timeout")
	$CountdownLabel.show()
	$StartButton.hide()
	$StreakNotification.hide()
	emit_signal("start_countdown")


func tutorial(tutorialPlaying):
	if(tutorialPlaying):
		$StartButton.hide()
		$StreakNotification.hide()
		$PauseMenu/CheckQuit.hide()
		$PauseMenu.hide()
		$PauseButton.hide()
		$CountdownLabel.hide()
		$StreakModButton.hide()
		$ScoreLabel.hide()
		$StreakLabel.hide()
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func metronomePaused():
	TimerCount = beatsInPar
	$StreakModButton.hide()
	$CountdownLabel.show()
	$StartButton.hide()
	$StreakNotification.hide()
	emit_signal("start_countdown")


# Start the countdown
func song_countdown(bpm, isStart):
	print("countdown time")
	emit_signal("sound_metronome")
	
	# Start a new one second timer
	CountdownTimer = Timer.new()
	add_child(CountdownTimer)
	var BeatLength = 1/(bpm/60.0)
	CountdownTimer.autostart = true
	CountdownTimer.wait_time = BeatLength
	
	if(isStart):
		CountdownTimer.connect("timeout", self, "_timeout")	
	else:
		CountdownTimer.connect("timeout", self, "pauseTimeout")	
		
	CountdownTimer.start()

func stageIncrease(stage):
	$ProgressBar/StageLabel.text = "Stage " + String(stage)
	$AnimationPlayer.play("StageUpComplete")

func prepStage():
	$AnimationPlayer.play("StageUp")
	$ProgressBar/StageLabel.text = "Get Ready!"
	
func pauseTimeout():
	print("TimerGone")
	# Decrement the timer amount
	TimerCount -= 1
	# If timer has run out, start the song
	if TimerCount == 0:
		get_tree().paused = false
		$CountdownLabel.hide()
		CountdownTimer.stop()
		showMultiplier()
	else:
		emit_signal("sound_metronome")
		$CountdownLabel.text = String((beatsInPar + 1) - TimerCount)



# On timer timeout
func _timeout():
	print("TimerGone")
	# Decrement the timer amount
	TimerCount -= 1
	# If timer has run out, start the song
	if TimerCount == 0:
		emit_signal("start_song")
		$CountdownLabel.hide()
		CountdownTimer.stop()
		showMultiplier()
	else:
		emit_signal("sound_metronome")
		$CountdownLabel.text = String((beatsInPar + 1) - TimerCount)


func showMultiplier():
	$StreakModButton.show()
	$PauseButton.show()
	$AnimationPlayer.play("start")


func streakNotification(var notes):
	$StreakNotification.show()
	$AnimationPlayer.play("StreakUp")
	$StreakNotification.text = (notes as String) + " NOTE STREAK!"
	yield(get_tree().create_timer(3.5), "timeout")
	hideStreakNotification()

func hideStreakNotification():
	$AnimationPlayer.play("StreakDown")


func hideProgress():
	$ProgressBar.hide()





func gameOver():
	Save.saveDifficulties(PlayerInfo.activeDifficulties)
	$PauseButton.hide()
	$PauseMenu.hide()
	get_tree().paused = false
	$AnimationPlayer.play("Failed")


func _on_StartButton_pressed():
	pass
	$StartButton.hide()
	emit_signal("start_countdown")
	
func update_streak(streak):
	$StreakLabel.text = str("Streak: ", streak)

func update_score(score):
	$ScoreLabel.text = str("Score: ", score)


func _on_hitIcons_toggled(button_pressed):
	$clicks.play()
	if(button_pressed):
		get_tree().get_root().get_node("PlayScene").get_node("Scoring").drawClickedIcons = true
		get_tree().get_root().get_node("PlayScene").get_node("Scoring").scoreModifier -= 0.2
	else:
		get_tree().get_root().get_node("PlayScene").get_node("Scoring").clearClickedIcons()
		get_tree().get_root().get_node("PlayScene").get_node("Scoring").drawClickedIcons = false
		get_tree().get_root().get_node("PlayScene").get_node("Scoring").scoreModifier += 0.2


func _on_timeHelper_toggled(button_pressed):
	$clicks.play()
	if(button_pressed):
		get_tree().get_root().get_node("PlayScene/Pointer").show()
		get_tree().get_root().get_node("PlayScene").get_node("Scoring").scoreModifier -= 0.4
	else:
		get_tree().get_root().get_node("PlayScene/Pointer").hide()
		get_tree().get_root().get_node("PlayScene").get_node("Scoring").scoreModifier += 0.4


func _on_Continue_pressed():
	metronomePaused()
	$PauseMenu.hide()
	$clicks.play()
	#$PauseButton.show()


func _on_Retry_pressed():
	var playScene = get_tree().root.get_node("PlayScene").PlayScene.instance()
	get_parent().add_child(playScene)
	playScene.init($"../".songJSON)
	self.queue_free()


func _on_Quit_pressed():
	Save.saveDifficulties(PlayerInfo.activeDifficulties)
	Save.update_difficulties()
	get_tree().paused = false
	get_tree().root.get_node("PlayScene").queue_free()
	self.queue_free()
	
	if(get_tree().root.get_node("PlayScene").exerciseType == "practice"):
		get_tree().change_scene("res://scenes/ChooseExercise/ChooseExercise.tscn")
	else:
		get_tree().change_scene("res://scenes/ChooseSong/ChooseSong.tscn")



func _on_PauseButton_pressed():
	$PauseMenu.show()
	$PauseButton.hide()
	$clicks.play()
	
	if(get_tree().root.get_node("PlayScene").exerciseType != "practice"):
		$PauseMenu/ExerciseHelp.hide()
	else:
		$PauseMenu/ExerciseHelp/ExerciseDescription.text = $"../".description
		$PauseMenu/ExerciseHelp/Note.texture = load($"../".noteImage)
		$PauseMenu/ExerciseHelp/Rest.texture = load($"../".restImage)
		
	if(!$"../".easyReadMode):
		$PauseMenu/timeHelper.hide()
		$PauseMenu/hitIcons.hide()
		
	get_tree().paused = true



func _on_hitIcons_pressed():
	pass # Replace with function body.


func _on_OpenCheckQuit_pressed():
	$clicks.play()
	$PauseMenu/CheckQuit.show()

func _on_CloseCheck_pressed():
	$clicks.play()
	$PauseMenu/CheckQuit.hide()
