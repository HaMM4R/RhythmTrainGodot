extends Node2D

var songFailed = false
var score = 0.0
var baseScoreIncrease = 10
var streak = 0
var max_streak = 0
var streakModifier = 1
var scoreModifier = 1

var notes_hit = 0
var totalNotes = 0

var totalNoteDiff = 0

var inNote
# Stores where the note is clicked
var clickedNote = false
var canHitNotes = false
var curNote = 0

var alwaysHitNotes = false

onready var GUI = $"../GUI"
onready var pointer = $"../Pointer"

var missedSounds = [
	load("res://assets/sounds/Misses/missed1.mp3"),
	load("res://assets/sounds/Misses/missed2.mp3"),
	load("res://assets/sounds/Misses/missed3.mp3"),
	load("res://assets/sounds/Misses/missed3.mp3"),
	load("res://assets/sounds/Misses/missed4.mp3"),
	load("res://assets/sounds/Misses/missed5.mp3")
]

var multiplierUpSounds = [
	load("res://assets/sounds/Multiplier/MultiplierUp1.mp3"),
	load("res://assets/sounds/Multiplier/MultiplierUp2.mp3"),
	load("res://assets/sounds/Multiplier/MultiplierUp3.mp3")
]

var multiplierDownSound = load("res://assets/sounds/Multiplier/MultiplierDown.mp3")

var timeClickIcon = load("res://scenes/ClickedNote/ClickedNote.tscn")
var clickedIcons = []
var drawClickedIcons = true


#Check for clicked note
func noteClicked():
	if !canHitNotes or songFailed:
		return
	
	var hasHitNote = false 
	
	var noteHit = pointer.getNote()
	
	if noteHit == null:
		return

	
	if inNote:
		if	!clickedNote:
			clickedNote = true
			if noteHit != null:
				if !noteHit.isRest:
					noteHitSuccess(noteHit)
					handleClickedIcons(true)
					$"../".noteHit(true, noteHit)

				else:
					noteHitFail(noteHit)
					handleClickedIcons(false)
					$"../".noteHit(false, noteHit)
	else:
		noteHitFail(noteHit)
		handleClickedIcons(false)
		$"../".noteHit(false, noteHit)


func handleClickedIcons(hasHit):
	if($"../".easyReadMode):
		var icon = timeClickIcon.instance()
		icon.position.x = pointer.position.x
		icon.position.y = pointer.position.y
		
		if hasHit:
			icon.get_node("Sprite").modulate = Color(0,159,255,1)
			icon.get_node("AnimationPlayer").play("Hit")
		else:
			icon.get_node("Sprite").modulate = Color(255,0,0,1)
			icon.get_node("AnimationPlayer").play("Miss")

		if(drawClickedIcons):
			add_child(icon)
			clickedIcons.append(icon)



#Check for passed note
func notePassed(var noteHit):
	if(!is_instance_valid(noteHit)):
		return 
		
	if(noteHit.noteClicked):
		return 
	
	if(!alwaysHitNotes):
		totalNotes += 1
	
	if(alwaysHitNotes):
		clickedNote = true 
		if(!noteHit.isRest):
			handleClickedIcons(true)
		$"../".noteHit(true, noteHit)
		noteHitSuccess(noteHit)
		
	if noteHit.isRest:
		if !clickedNote:
			clickedNote = true 
			$"../".noteHit(true, noteHit)
			noteHitSuccess(noteHit)
	else:
		if !noteHit.noteClicked:
			$"../".noteHit(false, noteHit)
			noteHitFail(noteHit)

	clickedNote = true


func incrementPoints():
	score += (1 * streakModifier)
	GUI.update_score(score)

func noteHitSuccess(var noteHit):
	if noteHit.noteClicked or songFailed:
		return
	
	if(!noteHit.isRest):
		$HitSound.pitch_scale = randf()*0.75+0.5
		$HitSound.play()
		if(!alwaysHitNotes):
			totalNotes += 1
	
	
	if(alwaysHitNotes):	
		noteHit.animateNote()
		clickedNote = true
		noteHit.noteClicked = true
		return
		
	noteHit.animateNote()
	streak += 1
	$"../".notesHitInBar += 1

				
	if streak > 1 and streakModifier < 4 and streak % 5 == 1:
		$MultiplierSound.stream = multiplierUpSounds[streakModifier - 1]
		$MultiplierSound.play()
		streakModifier+=1
		GUI.get_node("AnimationPlayer").play("MultiplierIncreased")
		GUI.get_node("StreakModButton/modlabel").text = str("x",streakModifier)
	
	
	if((streak + 1) % 20 == 1):
		GUI.streakNotification(streak)
		$StreakSound.pitch_scale = randf()*0.9+1
		$StreakSound.play()
		
	clickedNote = true
	noteHit.noteClicked = true
	
	# Increment score, score modifier is for penalties for having the pointer or low bpm etc

	if($"../".exerciseType == "practice"):
		score += ((float(baseScoreIncrease) * float(streakModifier)) * float(scoreModifier))
		GUI.update_score(score)
	
	GUI.update_streak(streak)
	
	if streak > max_streak:
		max_streak = streak
	
	var timeNote = noteHit.timeInSong 
	var timeNow = $"../MusicPlayer".get_playback_position()
	var diff = timeNote - timeNow
	GUI.get_node("DiffLabel").text = str(stepify(diff,0.01), "s")
	
	notes_hit+=1
	totalNoteDiff+=abs(diff)
	


#If note isn't hit successfully 
func noteHitFail(noteHit):
	if(songFailed):
		return 
	
	if streak > max_streak:
		max_streak = streak
	
	if(streakModifier != 1):
		$MultiplierSound.stream = multiplierDownSound
		$MultiplierSound.play()
		GUI.get_node("AnimationPlayer").play("MultiplierDecreased")
	
	noteHit.gameOver()
	noteHit.noteClicked = true
	
	streak = 0
	streakModifier = 1
	GUI.get_node("StreakModButton/modlabel").text = str("x",streakModifier)
	GUI.update_score(score)
	GUI.update_streak(streak)
	
	var missedSound = randi()% 5
	$MissedSound.stream = missedSounds[missedSound]
	$MissedSound.play()


var totalStop = false
func _on_Pointer_note_entered():
	# Flagging if inside note
	inNote = true
	clickedNote = false

	# Sets the current note in the bar and checks for clicks
	$"../".note_nodes[curNote].connect("clicked_note", self, "note_area_clicked")
	curNote += 1


# Function for when pointer exits note
# If a the note is missed, the streak is reset
func _on_Pointer_note_exited():	
	inNote = false
		
	var noteHit = pointer.getNote()
	notePassed(noteHit)



func clearClickedIcons():
	for icon in clickedIcons:
		icon.queue_free()
	# Clear the note array to make way for new notes
	clickedIcons.clear()

var stage = 1
func callResultsScreen(type, stageIn):
	stage = stageIn
	if(notes_hit == 0):
		show_results_screen(1, type)
	elif streak > max_streak:
		show_results_screen(totalNoteDiff/notes_hit, type)
	else:
		show_results_screen(totalNoteDiff/notes_hit, type)


func show_results_screen(var accuracy, type):
	var resultsScreen = load("res://scenes/ResultsNew/ResultsNew.tscn")
	var resultsInstance = resultsScreen.instance()
	resultsInstance.init(score, max_streak, accuracy, notes_hit, $"../".songID, true, totalNotes, false, $"../".songJSON, type, $"../".easyReadMode, $"../".bpm, $"../".allStageBarJSON, stage)
	get_tree().root.call_deferred("add_child", resultsInstance)
	$"../".queue_free() 


