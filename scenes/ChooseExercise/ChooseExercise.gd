extends Node2D

var songSelectButtons = []

var songStartButtons = []
var songPanels = []

var Files = []
var JsonData = []
var bpm = 80


var selectedIndex = 0
export (PackedScene) var PlayScene

var restImg = ""
var noteImg = ""
func _ready():
	$clicks.play()
	
	#Setup active buttons to allow the player to choose a song.
	songStartButtons = get_tree().get_nodes_in_group("songSelected")
	for i in range(0, songStartButtons.size()):
		songStartButtons[i].connect("pressed", self, "startSong", [i])	
		
	songPanels = get_tree().get_nodes_in_group("songPanel")

	var fileStart = "res://assets/exercises/"
	Files = GetFiles.get_files("res://assets/exercises")
	
	for i in range(Files.size()):
		var file = File.new()
		file.open(str(fileStart, Files[i]), File.READ)
		var text = file.get_as_text()
		file.close()
		
		# Parse text as Json
		var musicInfo = JSON.parse(text).result
		JsonData.append(musicInfo)
		# Song name?
		var songName = musicInfo["name"]
		var songID = musicInfo["id"]
		print("Song name: ",songName)

		songPanels[i].get_node("SongInfo/SongDesc").text = musicInfo["name"]
		songPanels[i].get_node("SongInfo/SongBPM").text = musicInfo["description"]
		

	
func startSong(id):
	selectedIndex = id
	$StartSong.show()
	$clicks.play()
	noteImg = JsonData[selectedIndex]["noteIconLocation"]
	restImg = JsonData[selectedIndex]["restIconLocation"]



func _on_CloseStart_pressed():
	$StartSong.hide()
	$clicks.play()

func _on_StartRead_pressed():
	if(songPanels[selectedIndex].get_node("SongInfo/bpm1").is_pressed()):
		bpm = 80
	if(songPanels[selectedIndex].get_node("SongInfo/bpm2").is_pressed()):
		bpm = 100
	if(songPanels[selectedIndex].get_node("SongInfo/bpm3").is_pressed()):
		bpm = 120
	$clicks.play()
	var easyRead = true
	yield(get_tree().create_timer(0.15), "timeout")	
	var playScene = PlayScene.instance()
	get_parent().add_child(playScene)
	playScene.init(JsonData[selectedIndex], bpm, easyRead, songPanels[selectedIndex].get_node("SongInfo/SongBPM").text, restImg, noteImg, JsonData)
	self.queue_free()


func _on_StartNoRead_pressed():
	if(songPanels[selectedIndex].get_node("SongInfo/bpm1").is_pressed()):
		bpm = 80
	if(songPanels[selectedIndex].get_node("SongInfo/bpm2").is_pressed()):
		bpm = 100
	if(songPanels[selectedIndex].get_node("SongInfo/bpm3").is_pressed()):
		bpm = 120
	$clicks.play()
	var easyRead = false
	yield(get_tree().create_timer(0.15), "timeout")	
	var playScene = PlayScene.instance()
	get_parent().add_child(playScene)
	playScene.init(JsonData[selectedIndex], bpm, easyRead, songPanels[selectedIndex].get_node("SongInfo/SongBPM").text, restImg, noteImg, JsonData)
	self.queue_free()


func _on_Back_pressed():
	get_tree().change_scene("res://scenes/MainNew/Main.tscn")


