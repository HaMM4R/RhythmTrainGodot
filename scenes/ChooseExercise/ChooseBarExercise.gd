extends Node2D

var currentMaxDifficultyValue = 0
var currentMinDifficultyValue = 0 

var stageBars

func returnBar(music_json, difficultyVal):
	currentMaxDifficultyValue = difficultyVal
	var bar = [] 

	var maxBarDifficulty = getMaxBarBasedOnDifficulty(music_json)
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	
	var barSelected = rng.randi_range(0,maxBarDifficulty)
	bar = music_json["RandomBars"][barSelected]
	return bar



#Returns the max difficulty of the random bars that can be displayed
func getMaxBarBasedOnDifficulty(music):
	var maxBarDifficulty = 0

	for i in range(0,music["RandomBars"].size()):
		maxBarDifficulty = i 
		if music["RandomBars"][i]["difficulty"] > currentMaxDifficultyValue: 
			break

	return maxBarDifficulty


#Returns the min difficulty of the random bars that can be displayed
func getMinBarBasedOnDifficulty(music):
	var minBarDifficulty = 0

	for i in range(0,music["RandomBars"].size()):
		if music["RandomBars"][i]["difficulty"] <= currentMinDifficultyValue: 
			minBarDifficulty = i 
		else:
			break

	return minBarDifficulty
