extends VBoxContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal ButtonPressed(index)

onready var titleLabel = $Panel/MarginContainer/VBoxContainer/Label
onready var xpLabel = $Panel/MarginContainer/VBoxContainer/XPLabel
onready var starHolder = $Panel/MarginContainer/VBoxContainer/StarHolder
onready var star1 = $Panel/MarginContainer/VBoxContainer/StarHolder/Star1
onready var star2 = $Panel/MarginContainer/VBoxContainer/StarHolder/Star2
onready var star3 = $Panel/MarginContainer/VBoxContainer/StarHolder/Star3

var titleText = ""
var index = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func init(var titleIn, indexIn, xpReq, id):
	titleText = titleIn
	index = indexIn
	titleLabel.text = titleText
	
	xpLabel.hide()
	
	#wont let you play a song unless you have the right amount of XP 
	if(xpReq > PlayerInfo.totalPreviousScore):
		$Panel/MarginContainer/VBoxContainer/Button.hide()
		xpLabel.show()
		starHolder.hide()
		xpLabel.text = String(xpReq - PlayerInfo.totalPreviousScore) + "xp needed"
		
	var prevStarsForSong = int(Save.returnStars(id))
	
	#Turns the stars yellow if you've achieved them 
	if(prevStarsForSong == 3):
		star3.modulate = Color(1,1,1,1)
		star2.modulate = Color(1,1,1,1)
		star1.modulate = Color(1,1,1,1)
	elif prevStarsForSong == 2:
		star2.modulate = Color(1,1,1,1)
		star1.modulate = Color(1,1,1,1)
	elif prevStarsForSong == 1:
		star1.modulate = Color(1,1,1,1)



# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass





func _on_Button_button_down():
	emit_signal("ButtonPressed", index)
