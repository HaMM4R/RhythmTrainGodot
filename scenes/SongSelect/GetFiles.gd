extends Node


func get_files(var path):
	var files = []
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin()

	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif file.ends_with(".json"):
			files.append(file)
	dir.list_dir_end()

	return files


func getMusicJSON(beats):
	var file = File.new()
	file.open(str("res://assets/json/",beats), File.READ)
	var text = file.get_as_text()
	file.close()
	
	return JSON.parse(text).result
