extends VBoxContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

signal ButtonPressed(index, bpm)

onready var titleLabel = $Panel/MarginContainer/VBoxContainer/Label
onready var xpLabel = $Panel/MarginContainer/VBoxContainer/XPLabel


var titleText = ""
var index = 0
var bpm = 80

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func init(var titleIn, indexIn, id):
	titleText = titleIn
	index = indexIn
	titleLabel.text = titleText
	
	var prevStarsForSong = int(Save.returnStars(id))
	




# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass





func _on_Button_button_down():
	if($Panel/MarginContainer/VBoxContainer/CheckBox.is_pressed()):
		bpm = 80
	if($Panel/MarginContainer/VBoxContainer/CheckBox2.is_pressed()):
		bpm = 100
	if($Panel/MarginContainer/VBoxContainer/CheckBox3.is_pressed()):
		bpm = 120
		
	emit_signal("ButtonPressed", index, bpm)
	
	
	
