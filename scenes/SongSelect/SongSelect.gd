extends PanelContainer

export (PackedScene) var SongBox
export (PackedScene) var PlayScene
# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var Files = []

var JsonData = []

var selectedIndex = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	
	$VBoxContainer/HBoxContainer/xplabel.text = "Current XP: " + String(PlayerInfo.totalPreviousScore)
	$VBoxContainer/HBoxContainer/Level.text = "Level: " + String(PlayerInfo.currentLevel)
	$VBoxContainer/HBoxContainer/TextureRect.texture = PlayerInfo.levelBadges[PlayerInfo.currentLevel - 1]

	var fileStart = "res://assets/music/"
	Files = GetFiles.get_files("res://assets/music")
	
	for i in range(Files.size()):
		var file = File.new()
		file.open(str(fileStart, Files[i]), File.READ)
		var text = file.get_as_text()
		file.close()
		
		# Parse text as Json
		var musicInfo = JSON.parse(text).result
		JsonData.append(musicInfo)
		# Song name?
		var songName = musicInfo["name"]
		var requiredXP = musicInfo["xpRequirement"]
		var songID = musicInfo["id"]
		print("Song name: ",songName)
		
		var newSong = SongBox.instance()
		$SongScrollContainer/MarginContainer/SongHBox.add_child(newSong)
		newSong.init(songName, i, requiredXP, songID)
		newSong.connect("ButtonPressed", self, "_on_song_selected")
	
	$VBoxContainer/HBoxContainer/xplabel.margin_top = 0



func _on_song_selected(var index):
	var title = JsonData[index]["name"]
	var desc = JsonData[index]["description"]
	var bpm = JsonData[index]["bpm"]
	var beats = JsonData[index]["beats"]
	var id = JsonData[index]["id"]
	selectedIndex = index
	
	$CenterContainer/Popup/VBoxContainer/PopupDescLabel.text = desc
	$CenterContainer/Popup/VBoxContainer/HBoxContainer/PopupTitle.text = title
	$CenterContainer/Popup/VBoxContainer/StarRating.text = "Previous Stars: " + String(Save.returnStars(id))
	$CenterContainer/Popup.show()

	

func _on_PopupExit_pressed():
	$CenterContainer/Popup.hide()


func _on_PopupButton_pressed():
	var playScene = PlayScene.instance()
	get_parent().add_child(playScene)
	print(JsonData[selectedIndex])
	playScene.init(JsonData[selectedIndex])
	self.queue_free()


func _on_back_pressed():
	self.queue_free()
	get_tree().change_scene("res://scenes/MainMenu/MainMenu.tscn")
