extends GridContainer


# Declare member variables here. Examples:
# var a = 2
# var b = "text"

onready var nameLabel = $HBoxContainer/Name
onready var badge = $HBoxContainer/TextureRect
onready var activeExercises = $HBoxContainer/ActiveChallenge

# Called when the node enters the scene tree for the first time.
func _ready():
	#Sql.removed_save()
	Sql.signup_player()
	Sql.connect("player_saved", self, "setName")
	badge.hide()
	
	if(Save.returnActiveChallenge() == ""):
		activeExercises.hide()
	else:
		var activeChallenge = Save.returnActiveChallenge()
		activeExercises.text = activeChallenge + String(Save.returnChallengeDetails(activeChallenge))
	
	

func setName():
	if PlayerInfo.playerName != null:
		nameLabel.text = "Name: " + PlayerInfo.playerName
		
	#Sets the level of the player
	for i in range (0, PlayerInfo.xpForLevel.size()):
		if (PlayerInfo.totalPreviousScore < PlayerInfo.xpForLevel[i]):
			break
		PlayerInfo.currentLevel = i + 1
		
	Sql.update_level(PlayerInfo.currentLevel)
	#Set player badge
	badge.texture = PlayerInfo.levelBadges[PlayerInfo.currentLevel - 1]
	badge.show()


func _on_PlayButton_pressed():
	get_tree().change_scene("res://scenes/SongSelect/SongSelect.tscn")


func _on_Practice_pressed():
	get_tree().change_scene("res://scenes/SongSelect/ExerciseSelect.tscn")


func _on_Challenges_pressed():
	get_tree().change_scene("res://scenes/Challenges/Challenges.tscn")
