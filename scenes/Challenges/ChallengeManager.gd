extends Node

#Holds the id's for songs and exercises so can check if have met criteria 
var songIDs = [0,1,2,3,4,5,6,7,8,9,10,11, 16, 17]
var exerciseIDs = [12, 13, 14, 15]


##### All of this stuff really needs to go into JSON files #######
var challengeNames = [
	"Proficient Practice 1",
	"Star Student 1",
	"High Achiever 1",
	"Proficient Practice 2",
	"Star Student 2",
	"Hot Stuff 1",
	"Metronome Master 1",
	"High Achiever 2",
	"Metronome Master 2",
	"Hot Stuff 2"
]

var challengeDifficulties = [
	"Easy",
	"Easy",
	"Easy",
	"Medium",
	"Medium",
	"Medium",
	"Medium",
	"Hard",
	"Hard",
	"Hard",
]

var challengeMode = [
	"Practice",
	"Song",
	"Song",
	"Practice",
	"Song",
	"Song",
	"Song",
	"Song",
	"Song",
	"Song",
	"Song"
]

var challengeType = [
	"practice",
	"starRating",
	"scoreSong",
	"practice",
	"starRating",
	"streak",
	"accuracy",
	"scoreSong",
	"accuracy",
	"streak"
	
]

#Description for all the challenges
var challengeDescriptions = [
	#Easy
	"Complete three practice exercises",
	"Achieve a star rating in five songs",
	"Complete two songs with a score of over 1000 points",
	#Medium
	"Complete five practice exercises",
	"Achieve a three star rating on 3 songs",
	"Achieve a streak of 40 notes in three songs",
	"Get above 75% accuracy in 5 songs",
	#Hard
	"Complete 5 songs with a score of over 1500 points",
	"Get above 95% accuracy in 3 songs",
	"Achieve a streak of 100 notes in a song"
]

var playsForAchievement = [
	3,
	5,
	2,
	5,
	3,
	3,
	5,
	5,
	3,
	1
]

var valueForAchievement = [
	0,
	1,
	1000,
	0,
	3,
	40,
	75,
	1500,
	95,
	100
]


var challengeBadge = [
	load("res://assets/images/Medals/tick_silv.png"),
	load("res://assets/images/Medals/single_kill_silv.png"),
	load("res://assets/images/Medals/num_silv.png"),
	load("res://assets/images/Medals/tick_gold.png"),
	load("res://assets/images/Medals/triple_kill_gold.png"),
	load("res://assets/images/Medals/lit_silv.png"),
	load("res://assets/images/Medals/metro_silv.png"),
	load("res://assets/images/Medals/num_gold.png"),
	load("res://assets/images/Medals/metro_gold.png"),
	load("res://assets/images/Medals/lit_gold.png")
]

func checkCompletionOfChallenge(score, percentage, starRating, exerciseType, streak):
	var didCompleteNewChallenge = false
	var completedType1 = calculateChallengeProgress(score, "scoreSong")
	var completedType2 = calculateChallengeProgress(percentage, "accuracy")
	var completedType3 = calculateChallengeProgress(starRating, "starRating")
	var completedType4 = calculateChallengeProgress(streak, "streak")
	var completedType5 = false
	
	if(exerciseType == "practice"):
		completedType5 = calculateChallengeProgress(1, "practice")
	
	if(completedType1 or completedType2 or completedType3 or completedType4 or completedType5):
		didCompleteNewChallenge = true
	
	return didCompleteNewChallenge
		
func calculateChallengeProgress(val, type):
	var progress = 0
	var hasCompletedChallenge = false
	
	for i in range(0, challengeNames.size()):
		if(challengeType[i] != type):
			continue
			
		if(val >= valueForAchievement[i]):
			progress = Save.returnChallengeDetails(challengeNames[i])
			var progressAddition = 100 / playsForAchievement[i]
			
			if(progress < 100 and progress + progressAddition >= 100):
				hasCompletedChallenge = true
			
			if(progress <= 100):
				progress = progress + progressAddition + 1
				Save.saveChallengeDetails(challengeNames[i], progress)
				
	
	return hasCompletedChallenge












