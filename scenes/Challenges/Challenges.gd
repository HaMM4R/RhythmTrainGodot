extends Node2D


var challengeButtons = []
var curChallengeIndex = 0

func _ready():
	$ChallengeInfo.hide()
	$clicks.play()

	#Setup active buttons to allow the player to set an active challenge.
	challengeButtons = get_tree().get_nodes_in_group("challenge")
	for i in range(0, challengeButtons.size()):
		challengeButtons[i].connect("pressed", self, "challengeSelected", [i])
		var progress = Save.returnChallengeDetails(ChallengeManager.challengeNames[i])
		if(progress < 100):
			challengeButtons[i].text = ChallengeManager.challengeNames[i]
		else:
			challengeButtons[i].text = "Complete"



func _on_back_pressed():
	self.queue_free()
	get_tree().change_scene("res://scenes/MainNew/Main.tscn")


func challengeSelected(index):
	$clicks.play()
	$ChallengeInfo.show()
	$ChallengeInfo/SetActive.show()
	$ChallengeInfo/GotoExercises.hide()
	$ChallengeInfo/GotoSongs.show()
	$ChallengeInfo/CompleteLabel.hide()
	$ChallengeInfo/ActiveLabel.hide()
	$ChallengeInfo/GotoExercises.hide()
	$ChallengeInfo/GotoSongs.hide()
	
	if(ChallengeManager.challengeMode[curChallengeIndex] == "Practice"):
		$ChallengeInfo/GotoExercises.show()
	else:
		$ChallengeInfo/GotoSongs.show()
	
	curChallengeIndex = index
	$ChallengeInfo/ChallengeName.text = ChallengeManager.challengeNames[curChallengeIndex]
	$ChallengeInfo/ChallengeDesc.text = ChallengeManager.challengeDescriptions[curChallengeIndex]
	$ChallengeInfo/rewardPixture.texture = ChallengeManager.challengeBadge[curChallengeIndex]
	if(Save.returnActiveChallenge() == ChallengeManager.challengeNames[curChallengeIndex]):
		$ChallengeInfo/ActiveLabel.show()
		$ChallengeInfo/SetActive.hide()
	
	var progress = Save.returnChallengeDetails(ChallengeManager.challengeNames[curChallengeIndex])
	$ChallengeInfo/ProgressBar.value = progress
	
	if(progress >= 100):
		$ChallengeInfo/SetActive.hide()
		$ChallengeInfo/GotoExercises.hide()
		$ChallengeInfo/GotoSongs.hide()
		$ChallengeInfo/CompleteLabel.show()
		$ChallengeInfo/ActiveLabel.hide()


func _on_SetActive_pressed():
	$clicks.play()
	$ChallengeInfo/SetActive.hide()
	$ChallengeInfo/ActiveLabel.show()
	Save.saveActiveChallenge(ChallengeManager.challengeNames[curChallengeIndex])



func _on_GotoSongs_pressed():
	get_tree().change_scene("res://scenes/ChooseSong/ChooseSong.tscn")


func _on_GotoExercises_pressed():
	get_tree().change_scene("res://scenes/ChooseExercise/ChooseExercise.tscn")


func _on_Back_pressed():
	get_tree().change_scene("res://scenes/MainNew/Main.tscn")
