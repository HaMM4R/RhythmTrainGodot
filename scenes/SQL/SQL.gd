extends Node


var queryResult 
var leaderboardResults
var badgeResults

signal query_complete
signal player_saved
signal leaderboard_ready
signal completed_exercise

var isFirstCreation = false

#Session score is built up over over one session to save having to query the database every time we want to update
#the players score
var totalSessionScore = 0
onready var file = 'res://names.txt'


func _ready() -> void:
	Supabase.auth.connect("signed_in", self, "_on_signed_in")
	Supabase.auth.connect("signed_up", self, "_on_signed_up")
	Supabase.database.connect("selected", self, "_on_selected")
	Supabase.database.connect("error", self, "_on_error")
	Supabase.auth.connect("error", self, "_on_auth_error")
	
	var string = ["Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTYxNzAyNTk5OCwiZXhwIjoxOTMyNjAxOTk4fQ.PDl0fpInkOQhpO9EQMM-2P1NRtknG5kMLKJ0_vwXfoY"]
	Supabase.auth._bearer = string


func _on_selected(query_result) -> void: 
	queryResult = query_result
	emit_signal("query_complete")


func _on_error(error) -> void: 
	print(error)

	

#SIGNING UP
func signup_player(playerSaveFile, difficultyFile):
	#Check to see if the user already has information saved
	var save_game = File.new()
	if save_game.file_exists(playerSaveFile):
		Save.load_user(playerSaveFile, difficultyFile)
		return true
	#if not generate a random number for their name
	create_new_user(playerSaveFile, difficultyFile)
	return false

func create_new_user(playerSaveFile, difficultyFile):
	#creates player name and checks if already exists
	var name = yield(check_user_exists(), "completed")
	var leaderType = 1#yield(get_leaderboard_type(), "completed")

	#Create the user
	var user ={
		"name": name,
		"totalScore": 0,
		"totalStars": 0,
		"leaderboardType": leaderType,
	}
	
	isFirstCreation = true
	
	#save the user locally so they are able to retrieve results
	Save.save_user(user, playerSaveFile, difficultyFile)
	#insert into the database
	Supabase.database.query(SupabaseQuery.new().from("users").insert([user]))
	#get the player ID for inserting results later
	get_player_info(name)


func get_leaderboard_type():
	var query : SupabaseQuery = SupabaseQuery.new()
	query.from('leaderboardType').select(['*'])
	Supabase.database.query(query)
	
	#yield(get_tree().create_timer(1.0), "timeout")
	yield(self, "query_complete")
	print("has waiting?")
	var leaderboardTypes = queryResult
	
	#just holder values, this doesn't work because the leaderboard types
	#dont always return in the same order
	var postLeader = leaderboardTypes[0].numAssigned
	var prePostLeader = leaderboardTypes[1].numAssigned
	var noLeader = leaderboardTypes[2].numAssigned
	
	var postID = 0
	var prePostID = 0
	var noneID = 0
	
	#Had to get values in a for loop because it seemingly never returns the
	#results in the same order so couldn't just access directly, sure theres
	#a better way than this though 
	for i in range(0, leaderboardTypes.size()):
		if leaderboardTypes[i].id == 1:
			noLeader = leaderboardTypes[i].numAssigned
			noneID = leaderboardTypes[i].id
		if leaderboardTypes[i].id == 2:
			prePostLeader = leaderboardTypes[i].numAssigned
			prePostID = leaderboardTypes[i].id
		if leaderboardTypes[i].id == 3:
			postLeader = leaderboardTypes[i].numAssigned
			postID = leaderboardTypes[i].id
	
	#If all the same add to group one by default
	var lowestGroup = 1
	var val = noLeader + 1

	#Add users to the group with the least people
	if noLeader < prePostLeader  :
		lowestGroup = noneID
		val = noLeader + 1

	if prePostLeader < postLeader :
		lowestGroup = prePostID
		val = prePostLeader + 1

	if postLeader < noLeader :
		lowestGroup = postID
		val = postLeader + 1

	#post the update	
	var updateLeaderCount : SupabaseQuery = SupabaseQuery.new()
	updateLeaderCount.from("leaderboardType").update({"numAssigned": val}).eq("id", String(lowestGroup))
	Supabase.database.query(updateLeaderCount)
	
	print(leaderboardTypes)
	return lowestGroup

#Generate number for player name and check if already exists
func check_user_exists() -> int:
	#Generate initial random name
	var name = create_name()
	print(name, "- has name returned??")
	
	#Gets the current entries into the database
	var query : SupabaseQuery = SupabaseQuery.new()
	query.from('users').select(["name"])
	Supabase.database.query(query)

	#This yield really needs to be replaced to some form of await
	yield(self, "query_complete")
	var existingNames = queryResult

	var nameExists = true
	
	#Check if their name exists and if so assign a new one and cont
	#Really needs to be improved big time but should be okay for now
	while nameExists:
		nameExists = false
		for key in existingNames:
			if name == key.name:
				nameExists = true
				name = create_name()
				break

	return name

func create_name():
	var rng = RandomNumberGenerator.new()
	
	var f = File.new()
	f.open(file, File.READ)
	var index = 1
	#Check length of names list
	while not f.eof_reached(): 
		var line = f.get_line()
		line += " "
		index += 1
		
	#Go to start of file
	f.seek(0)
	
	#Get random name
	rng.randomize()
	var random = rng.randi_range(0,index)
	var name = ""

	for i in range(0, random):
		name = f.get_line()
	
	f.close()
	
	rng.randomize()
	var num = rng.randi_range(1000,9999)
	var finalName = name + String(num)
	print(finalName + " - finName")
	return finalName

#Get the playersID
func get_player_info(var pName):
	var query : SupabaseQuery = SupabaseQuery.new()
	query.from('users').select(['*']).eq("name", String(pName))
	Supabase.database.query(query)
	
	yield(self, "query_complete")
	#get the leaderboard group
	PlayerInfo.playerID = queryResult[0].id
	PlayerInfo.playerName = queryResult[0].name
	PlayerInfo.leaderboardGroup = queryResult[0].leaderboardType
	#Get the total previous score
	PlayerInfo.totalPreviousScore = queryResult[0].totalScore
	PlayerInfo.totalStars = queryResult[0].totalStars
	
	if(isFirstCreation):
		var rhythmSave = {
			"performances": Bayes.loadedResults,
			"playerID": PlayerInfo.playerID
		}
		
		var pushInitialPerformance : SupabaseQuery = SupabaseQuery.new()
		pushInitialPerformance.from("playerRhythmBayes").insert([rhythmSave])
		Supabase.database.query(pushInitialPerformance)
		isFirstCreation = false
	
	
	
	emit_signal("player_saved")
	#display this information


#To be called on completing an exercise
func completed_exercise(pScore, pAccuracy, pStreak, pNotesHit, pID, pFinalStatus):
	
	#Get attributes for storing
	var score = pScore
	score = int(score)
	
	var avgAccuracy = pAccuracy
	avgAccuracy = float(avgAccuracy)
	
	var maxStreak = pStreak
	maxStreak = int(maxStreak)
	
	var notesHit = pNotesHit
	notesHit = int(notesHit)
	
	var exerciseID = pID
	exerciseID = int(exerciseID)
	
	var finalStatus = pFinalStatus
	finalStatus = int(finalStatus)
	

	totalSessionScore += score
	#total score over time
	var totalScore = totalSessionScore + PlayerInfo.totalPreviousScore

	#create the session information
	#0 is passed for final status
	#1 is failed
	#2 is restarted
	var sessionDetails ={
		"playerID": PlayerInfo.playerID,
		"score": score,
		"maxStreak": maxStreak,
		"notesHit": notesHit,
		"avgAccuracy": avgAccuracy,
		"exerciseID": exerciseID, 
		"finishedStatus": finalStatus,
	}
	
	#save player score
	var query : SupabaseQuery = SupabaseQuery.new()
	query.from("scores").insert([sessionDetails])
	Supabase.database.query(query)
	
	#update the players total score
	var updateScore : SupabaseQuery = SupabaseQuery.new()
	updateScore.from("users").update({"totalScore": totalScore}).eq("id", String(PlayerInfo.playerID))
	Supabase.database.query(updateScore)
	
	update_difficulties()
	#update the leaderboard
	update_leaderboard(score, exerciseID)
	
	
func update_difficulties():
	var updateDifficulties : SupabaseQuery = SupabaseQuery.new()
	updateDifficulties.from("playerRhythmBayes").update({"performances": Bayes.loadedResults}).eq("playerID", String(PlayerInfo.playerID))
	Supabase.database.query(updateDifficulties)
	
func save_total_stars(stars):
	var updateStars : SupabaseQuery = SupabaseQuery.new()
	updateStars.from("users").update({"totalStars": stars}).eq("id", String(PlayerInfo.playerID))
	Supabase.database.query(updateStars)



#func testJSONSaving():
#	var test = {
#		"test": Bayes.loadedResults
#	}
#	var query : SupabaseQuery = SupabaseQuery.new()
#	query.from("jsonTest").insert([test])
#	Supabase.database.query(query)
#


func update_leaderboard(var score, var exerciseID):
	
	var hasRegisteredExercise = false	
	
	var leaderDetails ={
		"playerID": PlayerInfo.playerID,
		"score": score,
		"exerciseID": exerciseID,
		"playerName": PlayerInfo.playerName,
	}
	
	#Get existing result in the leaderboard
	var query : SupabaseQuery = SupabaseQuery.new()
	query.from('leaderboard').select(['*']).eq("playerID", String(PlayerInfo.playerID))
	Supabase.database.query(query)
	
	yield(self, "query_complete")

	#If the player hasn't yet registered a result, register one
	#If they have, update if the score is larger
	
	if queryResult.size() == 0:
		var insertScore : SupabaseQuery = SupabaseQuery.new()
		insertScore.from("leaderboard").insert([leaderDetails])
		Supabase.database.query(insertScore)
		return
		
	for key in queryResult:
		if key.exerciseID == exerciseID:
			hasRegisteredExercise = true
			
		if score > key.score and key.exerciseID == exerciseID:
			var updateScore : SupabaseQuery = SupabaseQuery.new()
			updateScore.from("leaderboard").update(leaderDetails).eq("playerID", String(PlayerInfo.playerID)).eq("exerciseID", String(exerciseID))
			Supabase.database.query(updateScore)
			return
			
	if !hasRegisteredExercise:
		var insertScore : SupabaseQuery = SupabaseQuery.new()
		insertScore.from("leaderboard").insert([leaderDetails])
		Supabase.database.query(insertScore)
		
	emit_signal("completed_exercise")




func print_leaderboard(var exerciseID):
	var leaderboardNames = []
	#var exerciseID = $exerciseIDFieldLeader.get_text()
	#exerciseID = int(exerciseID)
	var query : SupabaseQuery = SupabaseQuery.new()
	
	query.from('leaderboard').select(['*']).eq("exerciseID", String(exerciseID))
	Supabase.database.query(query)
	
	yield(self, "query_complete")
	#get the leaderboard group
	leaderboardResults = queryResult
	
	for i in range(0, leaderboardResults.size()):
		leaderboardNames.append(leaderboardResults[i].playerName)

	var query2 : SupabaseQuery = SupabaseQuery.new()
	query2.from('users').select(['*']).in("name", leaderboardNames)
	Supabase.database.query(query2)
	
	yield(self, "query_complete")
	badgeResults = queryResult
	
	emit_signal("leaderboard_ready")
	


func update_level(var level):
	var query : SupabaseQuery = SupabaseQuery.new()
	query.from('users').select(['*']).eq("name", String(PlayerInfo.playerName))
	Supabase.database.query(query)
	
	yield(self, "query_complete")

	var curLevel = queryResult[0].level
	if(curLevel < level):
		var updateScore : SupabaseQuery = SupabaseQuery.new()
		updateScore.from("users").update({"level": level}).eq("id", String(PlayerInfo.playerID))
		Supabase.database.query(updateScore)


func update_badge(badge):
	var updateScore : SupabaseQuery = SupabaseQuery.new()
	updateScore.from("users").update({"badge": badge}).eq("id", String(PlayerInfo.playerID))
	Supabase.database.query(updateScore)


func removed_save():
	var dir = Directory.new()
	dir.remove("user://savegame.save")
	dir.remove("user://difficulties.save")
