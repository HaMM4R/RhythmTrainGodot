extends CanvasLayer

var queryResult 
var playerName
var playerID
var leaderboardGroup
signal query_complete

#Total previous score is pulled from the database at start for the players total score
var totalPreviousScore
#Session score is built up over over one session to save having to query the database every time we want to update
#the players score
var totalSessionScore = 0
onready var file = 'res://names.txt'


func _ready() -> void:
	Supabase.auth.connect("signed_in", self, "_on_signed_in")
	Supabase.auth.connect("signed_up", self, "_on_signed_up")
	Supabase.database.connect("selected", self, "_on_selected")
	Supabase.database.connect("error", self, "_on_error")
	Supabase.auth.connect("error", self, "_on_auth_error")
	
	var string = ["Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJyb2xlIjoiYW5vbiIsImlhdCI6MTYxNzAyNTk5OCwiZXhwIjoxOTMyNjAxOTk4fQ.PDl0fpInkOQhpO9EQMM-2P1NRtknG5kMLKJ0_vwXfoY"]
	Supabase.auth._bearer = string


func _on_selected(query_result) -> void: 
	queryResult = query_result
	emit_signal("query_complete")


func _on_error(error) -> void: 
	print(error)

	

#SIGNING UP
func _on_Button2_pressed():
	#Check to see if the user already has information saved
	var save_game = File.new()
	if save_game.file_exists("user://savegame.save"):
		load_user()
		return
	
	#if not generate a random number for their name
	create_new_user()



func create_new_user():
	#creates player name and checks if already exists
	var name = yield(check_user_exists(), "completed")
	var leaderType = yield(get_leaderboard_type(), "completed")

	#Create the user
	var user ={
		"name": name,
		"totalScore": 0,
		"totalStars": 0,
		"leaderboardType": leaderType,
	}
	
	#save the user locally so they are able to retrieve results
	save_user(user)
	#insert into the database
	Supabase.database.query(SupabaseQuery.new().from("users").insert([user]))
	#get the player ID for inserting results later
	get_player_info(name)


func get_leaderboard_type():
	var query : SupabaseQuery = SupabaseQuery.new()
	query.from('leaderboardType').select(['*'])
	Supabase.database.query(query)
	
	#yield(get_tree().create_timer(1.0), "timeout")
	yield(self, "query_complete")
	print("has waiting?")
	var leaderboardTypes = queryResult
	
	#just holder values, this doesn't work because the leaderboard types
	#dont always return in the same order
	var postLeader = leaderboardTypes[0].numAssigned
	var prePostLeader = leaderboardTypes[1].numAssigned
	var noLeader = leaderboardTypes[2].numAssigned
	
	var postID = 0
	var prePostID = 0
	var noneID = 0
	
	#Had to get values in a for loop because it seemingly never returns the
	#results in the same order so couldn't just access directly, sure theres
	#a better way than this though 
	for i in range(0, leaderboardTypes.size()):
		if leaderboardTypes[i].id == 1:
			noLeader = leaderboardTypes[i].numAssigned
			noneID = leaderboardTypes[i].id
		if leaderboardTypes[i].id == 2:
			prePostLeader = leaderboardTypes[i].numAssigned
			prePostID = leaderboardTypes[i].id
		if leaderboardTypes[i].id == 3:
			postLeader = leaderboardTypes[i].numAssigned
			postID = leaderboardTypes[i].id
	
	#If all the same add to group one by default
	var lowestGroup = 1
	var val = noLeader + 1

	#Add users to the group with the least people
	if noLeader < prePostLeader  :
		lowestGroup = noneID
		val = noLeader + 1

	if prePostLeader < postLeader :
		lowestGroup = prePostID
		val = prePostLeader + 1

	if postLeader < noLeader :
		lowestGroup = postID
		val = postLeader + 1

	#post the update	
	var updateLeaderCount : SupabaseQuery = SupabaseQuery.new()
	updateLeaderCount.from("leaderboardType").update({"numAssigned": val}).eq("id", String(lowestGroup))
	Supabase.database.query(updateLeaderCount)
	
	print(leaderboardTypes)
	return lowestGroup

#Generate number for player name and check if already exists
func check_user_exists() -> int:

	#Generate initial random name
	
	var name = create_name()
	print(name, "- has name returned??")
	
	#Gets the current entries into the database
	var query : SupabaseQuery = SupabaseQuery.new()
	query.from('users').select(["name"])
	Supabase.database.query(query)

	#This yield really needs to be replaced to some form of await
	yield(self, "query_complete")
	var existingNames = queryResult

	var nameExists = true
	
	#Check if their name exists and if so assign a new one and cont
	#Really needs to be improved big time but should be okay for now
	while nameExists:
		nameExists = false
		for key in existingNames:
			if name == key.name:
				nameExists = true
				name = create_name()
				break

	return name

func create_name():
	var rng = RandomNumberGenerator.new()
	
	var f = File.new()
	f.open(file, File.READ)
	var index = 1
	#Check length of names list
	while not f.eof_reached(): 
		var line = f.get_line()
		line += " "
		index += 1
		
	#Go to start of file
	f.seek(0)
	
	#Get random name
	rng.randomize()
	var random = rng.randi_range(0,index)
	var name = ""

	for i in range(0, random):
		name = f.get_line()
	
	f.close()
	
	rng.randomize()
	var num = rng.randi_range(1000,9999)
	var finalName = name + String(num)
	print(finalName + " - finName")
	return finalName

#Get the playersID
func get_player_info(var pName):

	var query : SupabaseQuery = SupabaseQuery.new()
	query.from('users').select(['*']).eq("name", String(pName))
	Supabase.database.query(query)
	
	yield(self, "query_complete")
	#get the leaderboard group
	playerID = queryResult[0].id
	playerName = queryResult[0].name
	leaderboardGroup = queryResult[0].leaderboardType
	#Get the total previous score
	totalPreviousScore = queryResult[0].totalScore
	#display this information
	$playerName.text = "PlayerName: " + String(playerName)
	$playerID.text = "PlayerID: " + String(playerID)
	$leaderGroup.text = "LeaderboardGroup: " + String(leaderboardGroup)
	$totalScore.text = "TotalScore: " + String(totalPreviousScore)



#Save the users data locally
func save_user(var saveData):
	var save_game = File.new()
	save_game.open("user://savegame.save", File.WRITE)
	save_game.store_line(to_json(saveData))
	save_game.close()




#Load the users data
func load_user():
	var save_game = File.new()
	save_game.open("user://savegame.save", File.READ)
	while save_game.get_position() < save_game.get_len():
		# Get the saved dictionary from the next line in the save file
		var node_data = parse_json(save_game.get_line())
		playerName = node_data["name"]
		get_player_info(playerName)





#To be called on completing an exercise
func _on_CompleteExercise_pressed():
	
	#Get attributes for storing
	var score = $score.get_text()
	score = int(score)
	
	var avgAccuracy = $accuracyField.get_text()
	avgAccuracy = float(avgAccuracy)
	
	var maxStreak = $streakField.get_text()
	maxStreak = int(maxStreak)
	
	var notesHit = $notesHitField.get_text()
	notesHit = int(notesHit)
	
	var exerciseID = $exerciseIDField.get_text()
	exerciseID = int(exerciseID)
	
	var finalStatus = $finalStatus.get_text()
	finalStatus = int(finalStatus)
	

	totalSessionScore += score
	#total score over time
	var totalScore = totalSessionScore + totalPreviousScore

	#create the session information
	#0 is passed for final status
	#1 is failed
	#2 is restarted
	var sessionDetails ={
		"playerID": playerID,
		"score": score,
		"maxStreak": maxStreak,
		"notesHit": notesHit,
		"avgAccuracy": avgAccuracy,
		"exerciseID": exerciseID, 
		"finishedStatus": finalStatus,
	}
	
	#save player score
	var query : SupabaseQuery = SupabaseQuery.new()
	query.from("scores").insert([sessionDetails])
	Supabase.database.query(query)
	
	#update the players total score
	var updateScore : SupabaseQuery = SupabaseQuery.new()
	updateScore.from("users").update({"totalScore": totalScore}).eq("id", String(playerID))
	Supabase.database.query(updateScore)
	
	#update the leaderboard
	update_leaderboard(score, exerciseID)
	



func update_leaderboard(var score, var exerciseID):
	
	var hasRegisteredExercise = false	
	
	var leaderDetails ={
		"playerID": playerID,
		"score": score,
		"exerciseID": exerciseID,
		"playerName": playerName,
	}
	
	#Get existing result in the leaderboard
	var query : SupabaseQuery = SupabaseQuery.new()
	query.from('leaderboard').select(['*']).eq("playerID", String(playerID))
	Supabase.database.query(query)
	
	yield(self, "query_complete")

	#If the player hasn't yet registered a result, register one
	#If they have, update if the score is larger
	
	if queryResult.size() == 0:
		var insertScore : SupabaseQuery = SupabaseQuery.new()
		insertScore.from("leaderboard").insert([leaderDetails])
		Supabase.database.query(insertScore)
		return
		
	for key in queryResult:
		if key.exerciseID == exerciseID:
			hasRegisteredExercise = true
			
		if score > key.score and key.exerciseID == exerciseID:
			var updateScore : SupabaseQuery = SupabaseQuery.new()
			updateScore.from("leaderboard").update(leaderDetails).eq("playerID", String(playerID)).eq("exerciseID", String(exerciseID))
			Supabase.database.query(updateScore)
			return
			
	if !hasRegisteredExercise:
		var insertScore : SupabaseQuery = SupabaseQuery.new()
		insertScore.from("leaderboard").insert([leaderDetails])
		Supabase.database.query(insertScore)




func _on_printLeader_pressed():
	var exerciseID = $exerciseIDFieldLeader.get_text()
	exerciseID = int(exerciseID)
	var query : SupabaseQuery = SupabaseQuery.new()
	
	query.from('leaderboard').select(['*']).eq("exerciseID", String(exerciseID))
	Supabase.database.query(query)
	
	yield(self, "query_complete")
	#get the leaderboard group
	var leaderResults = queryResult
	print(leaderResults)
	


func _on_CreateNewUserDebug_pressed():
	totalPreviousScore = 0 
	totalSessionScore = 0
	
	create_new_user()


func _on_RemoveSave_pressed():
	var dir = Directory.new()
	dir.remove("user://savegame.save")


