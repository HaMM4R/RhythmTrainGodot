extends Node2D

var songStartButtons = []
var songLeaderboardButtons = [] 
var songPanels = [] 

var Files = []
var JsonData = []
var ExerciseJSON = []
var selectedIndex = 0
export (PackedScene) var PlayScene

var playerXPStart = 0
var playerXPAfterSong = 0

var songIDs = []

func _ready():
	#$SongInfo.hide()
	$clicks.play()
	$SongsUnlocked.hide()
	playerXPStart = PlayerInfo.totalPreviousScore
	
	Sql.signup_player(PlayerInfo.activeSave, PlayerInfo.activeDifficulties)
	Sql.connect("player_saved", self, "loadSongs")
	
	Bayes.recalculateInitialBeliefs()
	############## REMOVE THIS AT SOME POINT BECAUSE ITS UNNEEDED ####################
	var fileStart ="res://assets/exercises/"
	Files = GetFiles.get_files("res://assets/exercises")
	
	for i in range(Files.size()):
		var file = File.new()
		file.open(str(fileStart, Files[i]), File.READ)
		var text = file.get_as_text()
		file.close()
		
		# Parse text as Json
		var musicInfo = JSON.parse(text).result
		ExerciseJSON.append(musicInfo)
	####################################################################################
	
	#Setup active buttons to allow the player to choose a song.
	songStartButtons = get_tree().get_nodes_in_group("songSelected")
	for i in range(0, songStartButtons.size()):
		songStartButtons[i].connect("pressed", self, "startSong", [i])
	
	songLeaderboardButtons = get_tree().get_nodes_in_group("leaderboard")
	for i in range(0, songLeaderboardButtons.size()):
		songLeaderboardButtons[i].connect("pressed", self, "viewLeaderboard", [i])
	
	songPanels = get_tree().get_nodes_in_group("songPanel")
	for i in range(0, songPanels.size()):
		songPanels[i].hide()
	
	$LOADING.show()
	$StartSong.hide()
	#$HelpInfo.show()

func loadSongs():
	$LOADING.hide()
	
	var hasUnlockedNewSong = false
	playerXPAfterSong = PlayerInfo.totalPreviousScore

	for i in range(0, songPanels.size()):
		songPanels[i].show()
		
	var fileStart = "res://assets/music/"
	Files = GetFiles.get_files("res://assets/music")
	
	for i in range(Files.size()):
		var file = File.new()
		file.open(str(fileStart, Files[i]), File.READ)
		var text = file.get_as_text()
		file.close()
		
		# Parse text as Json
		var musicInfo = JSON.parse(text).result
		JsonData.append(musicInfo)
		# Song name?
		var songName = musicInfo["name"]
		var requiredXP = musicInfo["xpRequirement"]
		songIDs.append(musicInfo["id"])



		#NEW MENU STUFF####################################
		songPanels[i].get_node("SongInfo/SongDesc").text = musicInfo["description"]
		songPanels[i].get_node("SongInfo/SongBPM").text = str(musicInfo["bpm"]) + " bpm"
		
		if(PlayerInfo.totalPreviousScore >= requiredXP):	
			
			var prevStarsForSong = int(Save.returnStars(musicInfo["id"]))
			songPanels[i].get_node("SongInfo/star1").show()
			songPanels[i].get_node("SongInfo/star2").show()
			songPanels[i].get_node("SongInfo/star3").show()
			
			songPanels[i].get_node("SongInfo/XPToUnlock").hide()
			#Turns the stars yellow if you've achieved them 
			if(prevStarsForSong == 3):
				songPanels[i].get_node("SongInfo/star3").modulate = Color(1,1,1,1)
				songPanels[i].get_node("SongInfo/star2").modulate = Color(1,1,1,1)
				songPanels[i].get_node("SongInfo/star1").modulate = Color(1,1,1,1)
			elif prevStarsForSong == 2:
				songPanels[i].get_node("SongInfo/star2").modulate = Color(1,1,1,1)
				songPanels[i].get_node("SongInfo/star1").modulate = Color(1,1,1,1)
			elif prevStarsForSong == 1:
				songPanels[i].get_node("SongInfo/star1").modulate = Color(1,1,1,1)
		else:
			songPanels[i].get_node("SongInfo/star1").hide()
			songPanels[i].get_node("SongInfo/star2").hide()
			songPanels[i].get_node("SongInfo/star3").hide()
			songPanels[i].get_node("SongInfo/SongSelect").hide()
			songPanels[i].get_node("SongInfo/ViewLeaderboard").hide()
			songPanels[i].get_node("SongInfo/XPToUnlock").text = String(requiredXP - PlayerInfo.totalPreviousScore) + "xp to unlock!"
		#NEW MENU STUFF ###################################
		

			
		if(requiredXP > playerXPStart and requiredXP <= playerXPAfterSong and playerXPStart != playerXPAfterSong):
			hasUnlockedNewSong = true
			
	if(hasUnlockedNewSong):
		$SongsUnlocked.show()
		$songUnlockSound.play()
		
		



func startSong(id):
	selectedIndex = id
	$StartSong.show()
	$clicks.play()
	$MusicPreview.stream = load("res://assets/music/" + str(JsonData[selectedIndex]["file_name"]))
	$MusicPreview.play()
	
func viewLeaderboard(id):
	print("LEADERBOARD ID, ", id)
	queue_free()
	var leaderboardScreen = load("res://scenes/Leaderboard/Leaderboard.tscn")
	var leaderboardInstance = leaderboardScreen.instance()
	leaderboardInstance.init(songIDs[id])
	get_tree().root.call_deferred("add_child", leaderboardInstance)


func _on_Back_pressed():
	get_tree().change_scene("res://scenes/MainNew/Main.tscn")

func _on_ViewLeaderboard_pressed():
	queue_free()
	var leaderboardScreen = load("res://scenes/Leaderboard/Leaderboard.tscn")
	var leaderboardInstance = leaderboardScreen.instance()
	leaderboardInstance.init(songIDs[selectedIndex])
	get_tree().root.call_deferred("add_child", leaderboardInstance)


func _on_SongsUnlockedDismiss_pressed():
	$SongsUnlocked.hide()

func _on_CloseStart_pressed():
	$StartSong.hide()
	$clicks.play()

func _on_StartRead_pressed():
	$clicks.play()
	var easyRead = true
	#if($SongInfo/toggleread.is_pressed()):
#		easyRead = true
	yield(get_tree().create_timer(0.15), "timeout")	
	var playScene = PlayScene.instance()
	get_parent().add_child(playScene)
	playScene.init(JsonData[selectedIndex], easyRead, ExerciseJSON)
	self.queue_free()


func _on_StartNoRead_pressed():
	$clicks.play()
	var easyRead = false
	#if($SongInfo/toggleread.is_pressed()):
#		easyRead = true
	yield(get_tree().create_timer(0.15), "timeout")	
	var playScene = PlayScene.instance()
	get_parent().add_child(playScene)
	playScene.init(JsonData[selectedIndex], easyRead, ExerciseJSON)
	self.queue_free()
