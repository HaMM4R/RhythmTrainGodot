extends Node2D

var notesHit = 0
var score = 0
var songID
var songPassed
var streak = 0
var accuracy = 0
var totalNotes = 0
var songFailed = false
var bpm = 0
var starsAwarded = 0
var newStars = 0 

var startXPlevelup = false
var playerXPStart = 0
var playerLevelStart = 0
var playerFinalXPDifference = 0

var curLevelProgressPercentage = 100
var songJSON
var allStageBarJSON
var exerciseType = ""
var easyPlayMode = false
export (PackedScene) var PlayScene
export (PackedScene) var ExerciseScene

onready var stars = [
	$Panel/starHolder/star1,
	$Panel/starHolder/star2,
	$Panel/starHolder/star3
]

var starSounds = [
	load("res://assets/sounds/Multiplier/MultiplierUp1.mp3"),
	load("res://assets/sounds/Multiplier/MultiplierUp2.mp3"),
	load("res://assets/sounds/Multiplier/MultiplierUp3.mp3")
]

var didCompleteNewChallenge = false
var dismissedChallenge = false 

func _ready():
	$clicks.play()
	$ChallengeCompleted.hide()
	
	if(didCompleteNewChallenge):
		$ChallengeCompleted.show()
		$Panel/Continue.hide()
		$Panel/Restart.hide()
		$Stars.stream = starSounds[0]
		$Stars.play()
		
	#Set Labels
	setLabels()

	#Calculate values for initial XP bar
	if(PlayerInfo.currentLevel < 9):
		curLevelProgressPercentage = float(playerXPStart) / PlayerInfo.xpForLevel[playerLevelStart + 1] * 100 
	else:
		curLevelProgressPercentage = float(playerXPStart) / PlayerInfo.xpForLevel[9] * 100 
			
	var playerLevelWillBe = playerLevelStart
	
	#Calculate by how many percent the meter needs to increase by
	for i in range(PlayerInfo.currentLevel, PlayerInfo.xpForLevel.size()):
		if(PlayerInfo.totalPreviousScore + score > PlayerInfo.xpForLevel[i]):
			playerLevelWillBe += 1
	
	if(playerLevelStart < playerLevelWillBe):
		playerFinalXPDifference += (playerLevelWillBe - playerLevelStart) * 100

	if(PlayerInfo.currentLevel < 8):
		playerFinalXPDifference += float(PlayerInfo.totalPreviousScore + score) / PlayerInfo.xpForLevel[playerLevelWillBe + 1] * 100 
	elif(PlayerInfo.currentLevel < 9):
		playerFinalXPDifference += float(PlayerInfo.totalPreviousScore + score) / PlayerInfo.xpForLevel[9] * 100 
	#Set star icons
	if(songFailed):
		$Panel/starHolder.hide()
		yield(get_tree().create_timer(1.5), "timeout")	
	else:
		if(!didCompleteNewChallenge):
			for i in range(0, starsAwarded):
				yield(get_tree().create_timer(0.6), "timeout")	
				achieveStar(i)
			yield(get_tree().create_timer(1.5), "timeout")
			startXPlevelup = true	
			if(PlayerInfo.currentLevel < 9):
				$LevelUp.play()
		
		
	yield(get_tree().create_timer(5), "timeout")	
	$Leaderboard/LOADING.text = "LOADING FAILED, PLEASE CHECK SCORES VIA THE SONG SELECTION PAGE!"



func init(var scoreIn, var streakIn, var accuracyIn, var notesHitIn, var songIDIn, var songPassedIn, var totalNotesIn, var songFailedIn, songJSONIn, type, easyPlay, bpmIn, allJSON, stage):
	score = scoreIn
	notesHit = notesHitIn
	streak = streakIn
	accuracy = accuracyIn
	songID = songIDIn
	songPassed = songPassedIn
	totalNotes = totalNotesIn
	songFailed = songFailedIn		
	songJSON = songJSONIn
	exerciseType = type
	easyPlayMode = easyPlay
	bpm = bpmIn
	allStageBarJSON = allJSON
	
	playerXPStart = PlayerInfo.totalPreviousScore
	playerLevelStart = PlayerInfo.currentLevel
	
	
	if(!songFailed):
		var hitPercentage = float(notesHit) / float(totalNotes)
		
		#Calculate hit percentage to work out how many stars to award
		if(exerciseType != "song"):
			if hitPercentage > 0.85:
				starsAwarded = 3
			elif hitPercentage > 0.65:
				starsAwarded = 2
			elif hitPercentage > 0.25:
				starsAwarded = 1
		else:
			if stage > 7:
				starsAwarded = 3
			elif stage > 4:
				starsAwarded = 2
			elif stage > 1:
				starsAwarded = 1

		newStars = Save.saveSongDetails(songID, starsAwarded, score)
		didCompleteNewChallenge = ChallengeManager.checkCompletionOfChallenge(score, (hitPercentage * 100), starsAwarded, type, streak)
		print("DID COMPLETE", didCompleteNewChallenge)
	else:
		$MenuLabel.text = "Good Effort!"

	print(didCompleteNewChallenge)
	Sql.completed_exercise(score,accuracy,streak,notesHit,songID,songPassed)
	Sql.connect("completed_exercise", self, "loadLeaderboard")
	
	#SetStarColour

func loadLeaderboard():
	print("SONG HAS BEEN COMPLETED LOAD LEADERBOARD PLS PLS PLS ")
	$Leaderboard.init(songID)


func setLabels():
	#Set score label
	var noteHitPercentage = float(notesHit) / float(totalNotes) * 100
	var hitRounded = int(noteHitPercentage)
	
	if(exerciseType == "practice"):
		stars[0].get_node("Stage").text = "25%"
		stars[1].get_node("Stage").text = "65%"
		stars[2].get_node("Stage").text = "85%"	
		$Panel/Stage.text = "Achieve good accuracy to get stars!"
	
	
	if(!songFailed):
		$Panel/Score.text = String(score) + " (" + String(hitRounded) + "%)"
	else:
		$Panel/Score.text = String(score) 
		$Panel/LevelProgress.hide()
		$Panel/CurrentLevel.hide()
		$Panel/NextLevel.hide()
	
	#Set streaklabel
	$Panel/Streak.text = String(streak) + " note streak"
	
	#Set initial level progress
	$Panel/CurrentLevel.text = String(PlayerInfo.currentLevel)
	if(PlayerInfo.currentLevel < 9):
		$Panel/NextLevel.text = String(PlayerInfo.currentLevel + 1)
	else:
		$Panel/NextLevel.text = String(PlayerInfo.currentLevel)
		
	if(PlayerInfo.currentLevel < 9):
		$Panel/LevelProgress.value = float(playerXPStart) / PlayerInfo.xpForLevel[playerLevelStart + 1] * 100 
		curLevelProgressPercentage = float($Panel/LevelProgress.value)
	else:
		$Panel/LevelProgress.value = 100	



func achieveStar(index):
	stars[index].self_modulate = Color(1,1,1,1)
	$Stars.stream = starSounds[index]
	$Stars.play()
	
	if(index == 0):
		$AnimationPlayer.play("star1")
	if(index == 1):
		$AnimationPlayer.play("star2")
	if(index == 2):
		$AnimationPlayer.play("star3")
		
var levelIncrease = 0
func _physics_process(delta):
	
	if(curLevelProgressPercentage < playerFinalXPDifference and startXPlevelup):
		curLevelProgressPercentage += 0.08
		$Panel/LevelProgress.value = curLevelProgressPercentage
		
		if(curLevelProgressPercentage > 100 and playerLevelStart < 10):
			$LevelledUp.play()
			curLevelProgressPercentage = 0
			playerFinalXPDifference -= 100
			levelIncrease += 1
			if(playerLevelStart < 10):
				$Panel/CurrentLevel.text = String(PlayerInfo.currentLevel + levelIncrease)
				$Panel/NextLevel.text = String(PlayerInfo.currentLevel + levelIncrease + 1)
				
	elif startXPlevelup:
		startXPlevelup = false
		$LevelUp.stop()
	
	


func _on_Continue_pressed():
	$clicks.play()
	yield(get_tree().create_timer(0.15), "timeout")	
	self.queue_free()
	if(exerciseType == "practice"):
		get_tree().change_scene("res://scenes/ChooseExercise/ChooseExercise.tscn")
	else:
		get_tree().change_scene("res://scenes/ChooseSong/ChooseSong.tscn")


func _on_Restart_pressed():
	$clicks.play()
	yield(get_tree().create_timer(0.15), "timeout")	
	if(exerciseType == "practice"):
		var practiceScene = ExerciseScene.instance()
		get_parent().add_child(practiceScene)
		practiceScene.init(songJSON, bpm, easyPlayMode)
	else:
		var playScene = PlayScene.instance()
		get_parent().add_child(playScene)
		playScene.init(songJSON, easyPlayMode, allStageBarJSON)
	self.queue_free()


func _on_ChallengeDismiss_pressed():
	$ChallengeCompleted.hide()
	dismissedChallenge = true
	$Panel/Continue.show()
	$Panel/Restart.show()
	
	for i in range(0, starsAwarded):
		yield(get_tree().create_timer(0.6), "timeout")	
		achieveStar(i)
	yield(get_tree().create_timer(1.5), "timeout")
	startXPlevelup = true	
	if(PlayerInfo.currentLevel < 9):
		$LevelUp.play()
