
# RhythmTrain
RhythmTrain is an open-source mobile game, aiming to aid users in their practice of sight-reading rhythms in classic western music notation. The game is built in Godot and uses machine learning to adjust difficulty of the pseudo-random rhythms presented to the user in real-time. 

## How to Play
Playing RhythmTrain is as simple as tapping anywhere on the screen at the right time as denoted by the music notation. It offers two modes, the first facing users with a never ending, increasingly difficult set of rhythms to various songs and the second allowing players a safe environment to practice new rhythmic concepts that they may have never seen before, if you've never tried sight-reading before this might be a good place to start! 

**Please note**: RhythmTrain must have an online connection to function as intended. 

## Download
The APK can be downloaded from the release section either directly onto your Android device, or via a web browser on your desktop.

## Installation
Installation direct on Android device: 

- You can install APK files on your Android smartphone or tablet directly from your browser.
- On your devices browser, navigate to the releases for this project (or follow the google drive link directly below), then from here under assets, and other and tap on RhythmTrain.APK to be directed to a Google Drive download. From here you will be able to download RhythmTrain by tapping download – you should then be able to see it downloading on the top bar of your device.
- Once it's downloaded, using the file manager on your device, open Downloads, tap on the APK file and follow the installation instructions.
The app will begin installing on your device.

Installation from desktop machine is slightly more complex, we would invite you to follow the instructions found here:  https://www.wikihow.com/Install-APK-Files-from-a-PC-on-Android

Direct download link: https://drive.google.com/file/d/1uWBra9rMR1L9SaPsBr98gT40m7aOD5gk/view?usp=sharing

## Screenshots
![Game Image](https://i.imgur.com/EnnwGVI.jpg)

![Main Menu Image](https://i.imgur.com/JGDq5Ob.jpg)

![Results Image](https://i.imgur.com/OvrVpex.jpg)

Music used:
Bae Bae by Rameses B: https://soundcloud.com/ramesesb/rameses-b-bae-bae?in=royaltyfreemusic-nocopyrightmusic/sets/creative-commons-music

Insane by Ian Fever: https://soundcloud.com/ianfever/ian-fever-insane-original-mix?in=royaltyfreemusic-nocopyrightmusic/sets/creative-commons-music

Floating Through Time by Madbello: https://soundcloud.com/madbello/floating-through-time?in=royaltyfreemusic-nocopyrightmusic/sets/creative-commons-music

Memories by MusicByAden: https://soundcloud.com/musicbyaden/memories

Autumn by Ian Fever and Almi: https://soundcloud.com/ianfever/ian-fever-almi-autumn?in=royaltyfreemusic-nocopyrightmusic/sets/creative-commons-music

Destiny by Kontekst: https://soundcloud.com/kontekstmusic/destiny?in=royaltyfreemusic-nocopyrightmusic/sets/creative-commons-music

Heart by Ehrling: https://soundcloud.com/ehrling/ehrling-heart?in=royaltyfreemusic-nocopyrightmusic/sets/creative-commons-music

Spirit by Aritus: https://soundcloud.com/aritusmusic/aritus-spirit?in=royaltyfreemusic-nocopyrightmusic/sets/creative-commons-music

Cloud Nine by Prismic: https://soundcloud.com/prismicofficial/prismic-cloud-nine?in=royaltyfreemusic-nocopyrightmusic/sets/creative-commons-music

Endless Summer by Extenz: https://soundcloud.com/extenz/endless-summer?in=royaltyfreemusic-nocopyrightmusic/sets/creative-commons-music

Gravity by Extenz: https://soundcloud.com/extenz/gravity?in=royaltyfreemusic-nocopyrightmusic/sets/creative-commons-music

Colors by Markvard: https://soundcloud.com/markvard/markvard-colors?in=royaltyfreemusic-nocopyrightmusic/sets/creative-commons-music

Parallel by souKo: https://soundcloud.com/soukomusic/parallel?in=royaltyfreemusic-nocopyrightmusic/sets/creative-commons-music

Waiting for the Night by Electronic Senses: https://www.youtube.com/watch?v=KiswwlUSuJw&ab_channel=CCMusic

Parallel by souKo: https://soundcloud.com/soukomusic/parallel?in=royaltyfreemusic-nocopyrightmusic/sets/creative-commons-music

